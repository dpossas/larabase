<?php

namespace App\Http\Controllers\Basel5;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Cartalyst\Sentry\Facades\Laravel\Sentry;
use \App\Models\Basel5\User;

class ConfiguracaoBaseController extends Controller
{
    use \ForbiddenTrait;

    private $modulo = null;

    private $skins_opcoes = array();
    private $botoes_opcoes = array();
    private $colunaid_opcoes = array();
    private $smtpauth_opcoes = array();

   public function __construct()
    {
        //buscando o sistema basel5
        $base = \App\Models\Basel5\Sistema::where('nome','=','basel5')->firstOrFail();

        $this->modulo = \App\Models\Basel5\Modulo::where('nome','=','configuracao_base')
            ->where('sistema_id','=',$base->id)->firstOrFail();

        $this->skins_opcoes = [
            'smart-style-0' => 'Cinza Escuro (Padrão)',
            'smart-style-1' => 'Azul Escuro',
            'smart-style-2' => 'Leve',
            'smart-style-3' => 'Alaranjado',
            'smart-style-4' => 'Pixel Smart',
            'smart-style-5' => 'Glass'
        ];

        $this->botoes_opcoes = [
            0 => trans('lbl.semlegenda'),
            1 => trans('lbl.comlegenda')
        ];

        $this->colunaid_opcoes = [
            0 => trans('lbl.colunaid_ocultar'),
            1 => trans('lbl.colunaid_exibir')
        ];

        $this->smtpauth_opcoes = [
            'tls' => 'TLS',
            'ssl' => 'SSL'
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //acao não permitida
        return $this->acessoNegado();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //acao não permitida
        return $this->acessoNegado();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //acao não permitida
        return $this->acessoNegado();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //acao não permitida
        return $this->acessoNegado();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $item = \App\Models\Basel5\ConfiguracaoBase::firstOrFail();

            return view( 'backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit')
                ->with([
                    'item'=>$item,
                    'skins_opcoes'=>$this->skins_opcoes,
                    'botoes_opcoes'=>$this->botoes_opcoes,
                    'colunaid_opcoes'=>$this->colunaid_opcoes,
                    'smtpauth_opcoes'=>$this->smtpauth_opcoes
                ]);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $item = \App\Models\Basel5\ConfiguracaoBase::firstOrFail();
            $item->titulo = $request->get('titulo');
            $item->tipo_botao = $request->get('tipo_botao');
            $item->colunaid = $request->get('colunaid');
            $item->paginacao_registros = $request->get('paginacao_registros');
            $item->versao = $request->get('versao');
            $item->skin = $request->get('skin');
            $item->smtp_servidor = $request->get('smtp_servidor');
            $item->smtp_porta = $request->get('smtp_porta');
            $item->smtp_auth_type = $request->get('smtp_auth_type');
            $item->smtp_timeout = $request->get('smtp_timeout');
            $item->smtp_usuario = $request->get('smtp_usuario');
            if ($request->get('smtp_senha'))
                $item->smtp_senha = base64_encode($request->get('smtp_senha'));
            //$item->user_create_id = Sentry::getUser() && Sentry::getUser()->id ? Sentry::getUser()->id : User::firstOrFail()->id;
            $item->user_update_id = Sentry::getUser() && Sentry::getUser()->id ? Sentry::getUser()->id : User::firstOrFail()->id;

            if ( !$item->save() ) { 
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit'))))
                    ->withErrors([trans('message.saveerror')])
                    ->withInput() ;
            } else {
                $request->session()->flash('success', trans('messages.savesuccess'));

                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit'))));
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //acao não permitida
        return $this->acessoNegado();
    }
}
