<?php

namespace App\Http\Controllers\Basel5;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Cartalyst\Sentry\Facades\Laravel\Sentry;

class AdminController extends Controller
{
    public $gadm = null;

    public function __construct()
    {
        $sistemas = \Menu::retornaSistemas();
        $this->gadm = Sentry::findGroupByName('Administrador');
        view()->share('_sistemas',$sistemas);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Sentry::getUser() ){
            return view('larabase::backend.main');
        } else {
            return redirect(route('adm.login',[],false));
        }
        
    }

    public function login(){
        return view('larabase::backend.login');
    }
    
    public function trocaSistema(Request $request, $nome_sistema) 
    {
        session(['_s' => $nome_sistema]);
        return \Menu::renderizaMenu($nome_sistema);
    }

    public function postLogin(Request $request)
    {
        try {
            $email = $request->get('email');
            $password = $request->get('password');
            $remember = $request->get('remember');
            
            $credentials = array(
                'email' => $email,
                'password' => $password
                );

            $user = Sentry::authenticate($credentials,$remember);
            Sentry::login($user, $remember);
            
            if ( !$user->isSuperUser() && !$user->inGroup($this->gadm) ){
                Sentry::logout();
                return redirect()->route('adm.login');
            }
            
            return redirect()->route('adm.home');
        } catch (\Exception $e){
            session()->flash('loginerror',trans('message.dadosinvalidos'));
            return redirect()->route('adm.login')->withInput($request->except('password'));
        }
    }

    public function logout(){
        Sentry::logout();

        return redirect()->route('adm.login');
    }

    public function showMessage()
	{
		return view('larabase::backend.message')->with([
			'strmessage' => session('strmessage')
		]);
	}
}
