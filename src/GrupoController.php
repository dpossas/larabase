<?php

namespace App\Http\Controllers\Basel5;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use \Cartalyst\Sentry\Facades\Laravel\Sentry;
use \App\Models\Basel5\User;

class GrupoController extends Controller
{
    use \ForbiddenTrait;

    private $modulo = null;

    public function __construct(){
        //buscando o sistema basel5
        $base = \App\Models\Basel5\Sistema::where('nome','=','basel5')->first();

        $this->modulo = \App\Models\Basel5\Modulo::where('nome','=','grupo')
            ->where('sistema_id','=',$base->id)->firstOrFail();
    }
    
    public static function rotas()
    {
        return [
            ['method'=>'post','name'=>'data', 'alias'=>'dados']
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_acesso') ) ){
            return view('backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index' );
        } else {
            return $this->acessoNegado();
        }
    }

    public function data(Request $request)
    {
        if ( \Input::has('t') && \Input::get('t') === 'select2' ) {
            $result = Collection::make(DB::table('tabela')
                            ->select('id', 'name')
                            ->where('name','like','%'.\Input::get('q').'%')
                            ->orderBy('name','asc')
                            ->get())->keyBy('id');

            echo json_encode($result);

            return;
        }

        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_acesso') ) ){
            $fields = array(
                'id',
                'name'
                );
            $all = \App\Models\Basel5\Grupo::select($fields);

            if ( \Input::get('name') )
                $all->where('name','like','%'.\Input::get('name').'%');

            return \Datatables::of($all)->make(true);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_criacao') ) ){
            $listasistemas = \App\Models\Basel5\Sistema::orderBy('nome')->get();
            return view('backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')
                ->with(['listasistemas'=>$listasistemas]);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_criacao') ) ){
            $rules = array(
                'name'=>'required'
            );

            $validator = \Validator::make($request->all(), $rules);

            if ( $validator->fails() ){
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')),[],false))
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {
                    $permissions = array();
                    if ( $request->has('acesso') )
                        foreach ( $request->get('acesso') as $acesso){
                            $permissions[$acesso] = true;
                        }
                    if ( $request->has('inclusao') )
                        foreach ( $request->get('inclusao') as $inclusao){
                            $permissions[$inclusao] = true;
                        }
                    if ( $request->has('edicao') )
                        foreach ( $request->get('edicao') as $edicao){
                            $permissions[$edicao] = true;
                        }
                    if ( $request->has('exclusao') )
                        foreach ( $request->get('exclusao') as $exclusao){
                            $permissions[$exclusao] = true;
                        }

                    $group = Sentry::createGroup(array(
                        'name' => $request->get('name'),
                        'permissions' => $permissions
                    ));

                    return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false))
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } catch ( Exception $e ){
                    return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')),[],false))
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //acao não permitida
        return $this->acessoNegado();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $item = Sentry::findGroupById($id);
            $listasistemas = \App\Models\Basel5\Sistema::orderBy('nome')->get();

            return view( 'backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit')
                ->with([
                    'item'=>$item,
                    'listasistemas'=>$listasistemas
                ]);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $rules = array(
                'name'=>'required'
            );

            $validator = \Validator::make($request->all(), $rules);
            
            if ( $validator->fails() ){
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit',$id)),[],false))
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {
                    $item = Sentry::findGroupById($id);
                    $item->name = $request->get('name');
                    
                    $permissions = array();
                    if ( $request->has('acesso') )
                        foreach ( $request->get('acesso') as $acesso){
                            $permissions[$acesso] = true;
                        }
                    if ( $request->has('inclusao') )
                        foreach ( $request->get('inclusao') as $inclusao){
                            $permissions[$inclusao] = true;
                        }
                    if ( $request->has('edicao') )
                        foreach ( $request->get('edicao') as $edicao){
                            $permissions[$edicao] = true;
                        }
                    if ( $request->has('exclusao') )
                        foreach ( $request->get('exclusao') as $exclusao){
                            $permissions[$exclusao] = true;
                        }
                    $item->permissions = $permissions;

                    if ( !$item->save() ) { 
                        return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit',$id)),[],false))
                            ->withErrors([trans('messages.saveerror')])
                            ->withInput() ;
                    } else {
                        $request->session()->flash('success', trans('messages.savesuccess'));

                        return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false));
                    }
                } catch ( Exception $e ){
                    return redirect(route('adm.home',[],false).'/#',route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit',$id)),[],false))
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_exclusao') ) ){
            $item = \App\Models\Basel5\Grupo::find($id);
            
            if ( !$item ){
                $request->session()->flash('error', trans('messages.notfound'));
                return redirect(route('adm.home',[],false).'/#',route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false))
                        ->withInput(\Input::except('password'));
            } else {
                if ( !$item->delete() ){
                    $request->session()->flash('error', trans('messages.deleteerror'));
                } else {
                    $request->session()->flash('success', trans('messages.deletesuccess'));
                }
                return redirect(route('adm.home',[],false).'/#',route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit',$id)),[],false));
            }
        } else {
            return $this->acessoNegado();
        }
    }
}
