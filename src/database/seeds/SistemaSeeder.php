<?php

use Illuminate\Database\Seeder;

class SistemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$super 	= \DB::table('users')->where('permissions','like','%"superuser":1%')->first();

        $base   =   new \App\Models\Basel5\Sistema;
        $base->nome 			= 'basel5';
        $base->titulo			= 'Base L5';
        $base->prefixo_tabela	= 'basel5';
        $base->ativo 			= true;
        $base->system 			= true;
        $base->user_create_id	= $super->id;
        $base->user_update_id	= $super->id;
        $base->save();
    }
}
