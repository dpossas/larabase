<?php

use Illuminate\Database\Seeder;

class ModuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Buscando o primeiro superadmin que existir
        $super 	= \DB::table('users')->where('permissions','like','%"superuser":1%')->first();

        //buscando o sistema basel5
        $base = \App\Models\Basel5\Sistema::where('nome','=','basel5')->first();

        //modulo de apoio
        $moduloapoio = new \App\Models\Basel5\Modulo;
        $moduloapoio->sistema_id 			= $base->id;
        $moduloapoio->modulo_id 			= null;
        $moduloapoio->nome  				= 'apoio';
        $moduloapoio->titulo 				= 'Apoio';
        $moduloapoio->descricao 			= 'Opções de Apoio do Sistema Base L5';
        $moduloapoio->tabela 				= null;
        $moduloapoio->tipo					= 'ME';
        $moduloapoio->ativo 				= true;
        $moduloapoio->menu 					= true;
        $moduloapoio->customizado			= false;
        $moduloapoio->destino 				= null;
        $moduloapoio->janela_largura 		= null;
        $moduloapoio->janela_altura 		= null;
        $moduloapoio->paginacao 			= true;
        $moduloapoio->paginacao_registros 	= 20;
        $moduloapoio->modulo_clonagem_id 	= null;
        $moduloapoio->modulo_substituido_id = null;
        $moduloapoio->user_create_id 		= $super->id;
        $moduloapoio->user_update_id 		= $super->id;
        $moduloapoio->nova_janela			= false;
        $moduloapoio->icone                 = 'cloud';
        $moduloapoio->ordem                 = 0;
        $moduloapoio->save();

        //modulo de cadastros
        $modulocadastro = new \App\Models\Basel5\Modulo;
        $modulocadastro->sistema_id 			= $base->id;
        $modulocadastro->modulo_id 				= null;
        $modulocadastro->nome  					= 'cadastro';
        $modulocadastro->titulo 				= 'Cadastros';
        $modulocadastro->descricao 				= 'Cadastros do Sistema';
        $modulocadastro->tabela 				= null;
        $modulocadastro->tipo					= 'ME';
        $modulocadastro->ativo 					= true;
        $modulocadastro->menu 					= true;
        $modulocadastro->customizado			= false;
        $modulocadastro->destino 				= null;
        $modulocadastro->janela_largura 		= null;
        $modulocadastro->janela_altura 			= null;
        $modulocadastro->paginacao 				= true;
        $modulocadastro->paginacao_registros 	= 20;
        $modulocadastro->modulo_clonagem_id 	= null;
        $modulocadastro->modulo_substituido_id 	= null;
        $modulocadastro->user_create_id 		= $super->id;
        $modulocadastro->user_update_id 		= $super->id;
        $modulocadastro->nova_janela			= false;
        $modulocadastro->icone                  = 'folder';
        $modulocadastro->ordem                  = 1;
        $modulocadastro->save();


        //modulo de configuracao
        $moduloconfig = new \App\Models\Basel5\Modulo;
        $moduloconfig->sistema_id 				= $base->id;
        $moduloconfig->modulo_id 				= $moduloapoio->id;
        $moduloconfig->nome  					= 'configuracao_base';
        $moduloconfig->titulo 					= 'Configurações';
        $moduloconfig->descricao 				= 'Configurações Gerais do Sistema';
        $moduloconfig->tabela 					= 'basel5_configuracao';
        $moduloconfig->tipo						= 'CS'; //Cadastro Simples - Uma unica pagina
        $moduloconfig->ativo 					= true;
        $moduloconfig->menu 					= true;
        $moduloconfig->customizado				= false;
        $moduloconfig->destino 					= null;
        $moduloconfig->janela_largura 			= 700;
        $moduloconfig->janela_altura 			= 500;
        $moduloconfig->paginacao 				= true;
        $moduloconfig->paginacao_registros 		= 20;
        $moduloconfig->modulo_clonagem_id 		= null;
        $moduloconfig->modulo_substituido_id 	= null;
        $moduloconfig->user_create_id 			= $super->id;
        $moduloconfig->user_update_id 			= $super->id;
        $moduloconfig->nova_janela				= false;
        $moduloconfig->icone                    = 'cogs';
        $moduloconfig->ordem                    = 0;
        $moduloconfig->save();

        //modulo de grupo de usuarios
        $modulogrupo = new \App\Models\Basel5\Modulo;
        $modulogrupo->sistema_id 				= $base->id;
        $modulogrupo->modulo_id 				= $modulocadastro->id;
        $modulogrupo->nome  					= 'grupo';
        $modulogrupo->titulo 					= 'Grupos de Usuários';
        $modulogrupo->descricao 				= 'Cadastros de Grupos de Usuários do Sistema';
        $modulogrupo->tabela 					= 'groups';
        $modulogrupo->tipo						= 'CC'; //Cadastro Completo - CRUD
        $modulogrupo->ativo 					= true;
        $modulogrupo->menu 						= true;
        $modulogrupo->customizado				= false;
        $modulogrupo->destino 					= null;
        $modulogrupo->janela_largura 			= 700;
        $modulogrupo->janela_altura 			= 500;
        $modulogrupo->paginacao 				= true;
        $modulogrupo->paginacao_registros 		= 20;
        $modulogrupo->modulo_clonagem_id 		= null;
        $modulogrupo->modulo_substituido_id 	= null;
        $modulogrupo->user_create_id 			= $super->id;
        $modulogrupo->user_update_id 			= $super->id;
        $modulogrupo->nova_janela				= false;
        $modulogrupo->icone                     = 'users';
        $modulogrupo->ordem                     = 2;
        $modulogrupo->save();

        //modulo de usuarios
        $modulousuario = new \App\Models\Basel5\Modulo;
        $modulousuario->sistema_id 				= $base->id;
        $modulousuario->modulo_id 				= $modulocadastro->id;
        $modulousuario->nome  					= 'usuario';
        $modulousuario->titulo 					= 'Usuários';
        $modulousuario->descricao 				= 'Cadastros de Usuários do Sistema';
        $modulousuario->tabela 					= 'users';
        $modulousuario->tipo					= 'CC'; //Cadastro Completo - CRUD
        $modulousuario->ativo 					= true;
        $modulousuario->menu 					= true;
        $modulousuario->customizado				= false;
        $modulousuario->destino 				= null;
        $modulousuario->janela_largura 			= 700;
        $modulousuario->janela_altura 			= 500;
        $modulousuario->paginacao 				= true;
        $modulousuario->paginacao_registros 	= 20;
        $modulousuario->modulo_clonagem_id 		= null;
        $modulousuario->modulo_substituido_id 	= null;
        $modulousuario->user_create_id 			= $super->id;
        $modulousuario->user_update_id 			= $super->id;
        $modulousuario->nova_janela				= false;
        $modulousuario->icone                   = 'user';
        $modulousuario->ordem                   = 3;
        $modulousuario->save();

        //modulo de sistemas
        $modulosistemas = new \App\Models\Basel5\Modulo;
        $modulosistemas->sistema_id 			= $base->id;
        $modulosistemas->modulo_id 				= $modulocadastro->id;
        $modulosistemas->nome  					= 'sistema';
        $modulosistemas->titulo 				= 'Sistemas';
        $modulosistemas->descricao 				= 'Cadastros de Sistemas';
        $modulosistemas->tabela 				= 'basel5_sistema';
        $modulosistemas->tipo					= 'CC'; //Cadastro Completo - CRUD
        $modulosistemas->ativo 					= true;
        $modulosistemas->menu 					= true;
        $modulosistemas->customizado			= false;
        $modulosistemas->destino 				= null;
        $modulosistemas->janela_largura 		= 700;
        $modulosistemas->janela_altura 			= 500;
        $modulosistemas->paginacao 				= true;
        $modulosistemas->paginacao_registros 	= 20;
        $modulosistemas->modulo_clonagem_id 	= null;
        $modulosistemas->modulo_substituido_id 	= null;
        $modulosistemas->user_create_id 		= $super->id;
        $modulosistemas->user_update_id 		= $super->id;
        $modulosistemas->nova_janela			= false;
        $modulosistemas->icone                  = 'briefcase';
        $modulosistemas->ordem                  = 0;
        $modulosistemas->save();

        //modulo de módulos
        $modulomodulos = new \App\Models\Basel5\Modulo;
        $modulomodulos->sistema_id 				= $base->id;
        $modulomodulos->modulo_id 				= $modulocadastro->id;
        $modulomodulos->nome  					= 'modulo';
        $modulomodulos->titulo 					= 'Módulos';
        $modulomodulos->descricao 				= 'Cadastros de Módulos de Sistemas';
        $modulomodulos->tabela 					= 'basel5_modulo';
        $modulomodulos->tipo					= 'CC'; //Cadastro Completo - CRUD
        $modulomodulos->ativo 					= true;
        $modulomodulos->menu 					= true;
        $modulomodulos->customizado				= false;
        $modulomodulos->destino 				= null;
        $modulomodulos->janela_largura 			= 700;
        $modulomodulos->janela_altura 			= 500;
        $modulomodulos->paginacao 				= true;
        $modulomodulos->paginacao_registros 	= 20;
        $modulomodulos->modulo_clonagem_id 		= null;
        $modulomodulos->modulo_substituido_id 	= null;
        $modulomodulos->user_create_id 			= $super->id;
        $modulomodulos->user_update_id 			= $super->id;
        $modulomodulos->nova_janela				= false;
        $modulomodulos->icone                   = 'bookmark-o';
        $modulomodulos->icone                   = 1;
        $modulomodulos->save();
        
        //modulo de arquivos
        $moduloarquivos = new \App\Models\Basel5\Modulo;
        $moduloarquivos->sistema_id 			= $base->id;
        $moduloarquivos->modulo_id 				= $modulocadastro->id;
        $moduloarquivos->nome  					= 'arquivo';
        $moduloarquivos->titulo 				= 'Galeria de Arquivos';
        $moduloarquivos->descricao 				= 'Módulos de de uplod de Arquivos';
        $moduloarquivos->tabela 				= 'basel5_arquivo';
        $moduloarquivos->tipo					= 'CC'; //Cadastro Completo - CRUD
        $moduloarquivos->ativo 					= true;
        $moduloarquivos->menu 					= false;
        $moduloarquivos->customizado			= false;
        $moduloarquivos->destino 				= null;
        $moduloarquivos->janela_largura 		= 700;
        $moduloarquivos->janela_altura 			= 500;
        $moduloarquivos->paginacao 				= true;
        $moduloarquivos->paginacao_registros 	= 20;
        $moduloarquivos->modulo_clonagem_id 	= null;
        $moduloarquivos->modulo_substituido_id 	= null;
        $moduloarquivos->user_create_id 		= $super->id;
        $moduloarquivos->user_update_id 		= $super->id;
        $moduloarquivos->nova_janela			= false;
        $moduloarquivos->icone                  = 'file';
        $moduloarquivos->ordem                  = 999;
        $moduloarquivos->save();
    }
}
