<?php

use Illuminate\Database\Seeder;
use Cartalyst\Sentry\Facades\Laravel\Sentry;

class GrupoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sentry::createGroup(array(
		    'name'        => 'Administrador',
		    'permissions' => array(
		        
		    )
		));
    }
}
