<?php

use Illuminate\Database\Seeder;
use \App\Configuracao;

class ConfiguracaoBaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Buscando o primeiro superadmin que existir
        $super  = \DB::table('users')->where('permissions','like','%"superuser":1%')->first();
        
        $cg = new \App\Models\Basel5\ConfiguracaoBase;
        $cg->titulo             = 'BaseL5';
        $cg->versao             = '0.1';
        $cg->user_create_id     = $super->id;
        $cg->user_update_id     = $super->id;
        $cg->save();
    }
}
