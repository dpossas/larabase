<?php

use Illuminate\Database\Seeder;
use Cartalyst\Sentry\Facades\Laravel\Sentry;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super = Sentry::createUser(array(
	        'email'     => 'admin@webmoderna.com.br',
	        'password'  => 'q1w2e3r4@',
	        'activated' => true,
	        'permissions' => array(
	            'superuser' => 1
	        ),
	    ));
    }
}
