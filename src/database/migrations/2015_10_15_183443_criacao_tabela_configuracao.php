<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriacaoTabelaConfiguracao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basel5_configuracao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paginacao_registros')->default(20);
            $table->string('titulo',50);
            $table->string('versao',20);
            $table->boolean('tipo_botao')->default(true);
            $table->boolean('colunaid')->default(false);
            $table->enum('skin',['smart-style-0','smart-style-1','smart-style-2','smart-style-3','smart-style-4','smart-style-5'])->default('smart-style-0');

            $table->string('smtp_servidor',200)->nullable();
            $table->integer('smtp_porta')->nullable();
            $table->enum('smtp_auth_type',['TLS','SSL'])->nullable();
            $table->integer('smtp_timeout')->nullable();
            $table->string('smtp_usuario',200)->nullable();
            $table->string('smtp_senha',200)->nullable();

            $table->integer('user_create_id')->unsigned();
            $table->integer('user_update_id')->unsigned();

            $table->timestamps();

            $table->foreign('user_create_id')->references('id')->on('users');
            $table->foreign('user_update_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('basel5_configuracao');
    }
}
