<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriacaoTabelaSistema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basel5_sistema', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome',50)->unique();
            $table->string('titulo',100);
            $table->string('descricao',500)->nullable();
            $table->string('prefixo_tabela',10);
            $table->boolean('ativo')->default(true);
            $table->boolean('system')->default(false);
            $table->boolean('customizado')->default(false);
            $table->integer('user_create_id')->unsigned();
            $table->integer('user_update_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_create_id')->references('id')->on('users');
            $table->foreign('user_update_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basel5_sistema');
    }
}
