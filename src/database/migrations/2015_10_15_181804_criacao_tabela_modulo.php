<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriacaoTabelaModulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basel5_modulo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sistema_id')->unsigned();
            $table->integer('modulo_id')->unsigned()->nullable();
            $table->string('nome',100);
            $table->string('titulo',100);
            $table->text('descricao')->nullable();
            $table->string('tabela',200)->nullable();
            $table->enum('tipo',['CC','CL','ME','CS','SE','CI']);
            $table->boolean('ativo')->default(false);
            $table->boolean('menu')->default(true);
            $table->boolean('customizado')->default(false);
            $table->string('destino',200)->nullable();
            $table->integer('janela_largura')->nullable();
            $table->integer('janela_altura')->nullable();
            $table->boolean('paginacao')->default(true);
            $table->integer('paginacao_registros')->nullable();
            $table->integer('modulo_clonagem_id')->unsigned()->nullable();
            $table->integer('modulo_substituido_id')->unsigned()->nullable();
            $table->integer('user_create_id')->unsigned();
            $table->integer('user_update_id')->unsigned();
            $table->boolean('nova_janela')->default(false);
            $table->string('icone',20)->nullable();
            $table->timestamps();

            $table->foreign('sistema_id')->references('id')->on('basel5_sistema');
            $table->foreign('modulo_id')->references('id')->on('basel5_modulo');
            $table->foreign('modulo_clonagem_id')->references('id')->on('basel5_modulo');
            $table->foreign('modulo_substituido_id')->references('id')->on('basel5_modulo');
            $table->foreign('user_create_id')->references('id')->on('users');
            $table->foreign('user_update_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('basel5_modulo');
    }
}
