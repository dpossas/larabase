<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriacaoTabelaBasel5Arquivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basel5_arquivo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_original');
            $table->string('slug');
            $table->string('titulo');
            $table->string('texto_alternativo');
            $table->text('descricao')->nullable();
            $table->string('mimetype');
            $table->string('extensao_original');
            $table->integer('tamanho_byte');
            $table->integer('imagem_largura');
            $table->integer('imagem_altura');
            
            $table->integer('user_create_id')->unsigned();
            $table->integer('user_update_id')->unsigned();
            $table->integer('user_delete_id')->unsigned()->nullable();
            
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('user_create_id')->references('id')->on('users');
            $table->foreign('user_update_id')->references('id')->on('users');
            $table->foreign('user_delete_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('basel5_arquivo');
    }
}
