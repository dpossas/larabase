var modulo;

//Dropzone.autoDiscover = false;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    beforeSend: function(){
        //alert('Ainda não enviou!');
    },
    complete: function(){
        //alert('Ajax completo independente do tipo do retorno');
    },
    statusCode: {
        200: function(xhr, ajaxOptions) {
            $($('#content')).find('.alert').remove();
        },
        400: function(xhr, ajaxOptions, thrownError) {
            $($('#content')).find('.alert').remove();
            //criaAlerta(2,xhr.responseJSON||xhr.responseText,$('#content'));
        },
        404: function() {
            $($('#content')).find('.alert').remove();
        }
    }
});

$(document).ready(function(){
    $('body').on('click', 'a[data-method]', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var link = $(this);
        var httpMethod = link.data('method').toUpperCase();
        var form;
        if ($.inArray(httpMethod, ['PUT', 'DELETE']) === -1) {
            return;
        }
        if (httpMethod === 'DELETE') {
            $.confirm({
                theme: 'hololight',
                title: 'Confirmar ação!',
                content: '<b>Essa ação não poderá ser desfeita após a confirmação.</b> Tem certeza que deseja excluir esta informação?',
                confirmButton: 'Sim, tenho certeza!',
                cancelButton: 'Não',
                confirmButtonClass: 'btn-danger',
                cancelButtonClass: 'btn-default',
                backgroundDismiss: false,
                confirmKeys: [13], // ENTER key
                cancelKeys: [27], // ESC key
                confirm: function() {
                    form = $('<form>', {
                        'method': 'POST',
                        'action': link.attr('href')
                    });
                    var token = $('<input>', {
                        'type': 'hidden',
                        'name': '_token',
                        'value': $('meta[name=csrf-token]').prop('content')
                    });
                    var hiddenInput = $('<input>', {
                        'name': '_method',
                        'type': 'hidden',
                        'value': link.data('method')
                    });
                    form.append(token, hiddenInput).appendTo('body')
                    
                    if ( typeof carregaLista === 'function' ){
                        $.ajax({
                            type: form.attr('method'),
                            url: form.attr('action'),
                            dataType: 'html',
                            data: form.serialize(),
                            success: function( retorno ) {
                               carregaLista();
                            }
                        });
                        return true;
                    } else {
                        form.submit();
                    }
                },
                cancel: function() {
                    return true;
                }
            });
        } else {
            form = $('<form>', {
                'method': 'POST',
                'action': link.attr('href')
            });
            var token = $('<input>', {
                'type': 'hidden',
                'name': 'csrf_token',
                'value': $('meta[name=csrf_token]').prop('content')
            });
            var hiddenInput = $('<input>', {
                'name': '_method',
                'type': 'hidden',
                'value': link.data('method')
            });
            form.append(token, hiddenInput).appendTo('body')
            form.submit();
        }
    });

    $('#troca-sistema li a').click(function(e){
        e.preventDefault();

        var a = $(this);
        var url = a.prop('href');

        renderizaMenu(url);
    });

    $('body').on('click','.dataTable tbody tr td input[type=checkbox], .dataTable thead tr th input[type=checkbox]', function(e){
        e.stopPropagation();
        selectTr($(this).parents('table'));
        //selectTr($('.cmstable'));
    });

    $('body').on('change','select.recarrega', function(e){
        carregaLista($(this).val());
    });
    
    Dropzone.options.dpzonearquivo = {
        init: function() {
            this.on("queuecomplete", function() {
                $.ajax({
                    type: 'GET',
                    url: _urlarquivos,
                    dataType: 'html',
                    success: function( retorno ) {
                       $('#arquivos').empty().html(retorno);
                    }
                });
            }),
            this.on('success',function(file,arquivo){
                if ( typeof cbArquivo === 'function' ){
                    cbArquivo(arquivo);
                }
            })
        },
        url: '/admin/basel5/arquivo',
        dictDefaultMessage: 'Arraste os arquivos para cá (ou clique)'
    };
    
    formataNumeros();
});

function renderizaMenu(url){
    $.ajax({
        type: 'GET',
        url: url,
        success: function( retorno ) {
            var menu = $('ul#troca-sistema li a[href="'+url+'"');
            var txt = menu.text();
            var span = menu.parents('div').first().find('.project-selector').first();
            span.empty().html(txt+'<i class="fa fa-angle-down"></i>');

            $('#left-panel nav ul li:not(:first)').remove();
            $('#left-panel nav ul').append(retorno);

            $('nav ul').jarvismenu({
                accordion : true,
                speed : $.menu_speed,
                closedSign : '<em class="fa fa-plus-square-o"></em>',
                openedSign : '<em class="fa fa-minus-square-o"></em>'
            });
        }
    });
}

//metodos comuns
/**
* Muda o estilo da linha com checkbox selecionado
**/
function selectTr(table){
    table.find('tr').removeClass('selecionado');
    table.find('tr td input[type=checkbox]:checked').each(function(i, chk){
        $(chk).closest('tr').addClass('selecionado');
    });
}

/**
 * htmlLabel
 */
function htmlLabel(text,forInput) {
    var lbl = $('<label>',{
        html: text,
        for: forInput,
        id: 'lbl_'+forInput
    });
    
    return lbl;
}

/**
 * htmlSelect
 */
function htmlSelect(parameters,name,id,options) {
    var sel = $('<select></select> ',{
        name: name,
        id: id,
    });

    if ( parameters && parameters.class )
        sel.addClass(parameters.class);
    
    $(options).each(function(i,text){
        if ( text ){
            var opt = $('<option></option>',{
                value: i,
                html: text
            });
            
            if ( parameters && parameters.selectedValue && parameters.selectedValue == i )
                opt.attr('selected',true);

            opt.appendTo(sel);
        } 
    });
    
    return sel;
}

function selectTr(table){
    table.find('tr').removeClass('selecionado');
    table.find('tr td input[type=checkbox]:checked').each(function(i, chk){
        $(chk).closest('tr').addClass('selecionado');
    });
}

/**
 * Cria botao no padrao do template passando o texto, classe, icone e a posicao do icone
 */
function criaBotao(texto, type, id, classe, icone, posicao) {
    var button = $('<button></button>', {
        html: '&nbsp;&nbsp;' + texto,
        id: id,
        class: 'btn btn-' + classe
    });

    if (icone) {
        var i = $('<i></i>', {
            class: 'fa fa-' + icone
        });

        i.prependTo(button);
    }

    return button;
}

function formataNumeros() {
    $('.numerosemformatacao, .inteiro').autoNumeric('init',{
        aSep: '',
        mDec: '0'
    });
    
    $('.decimal2').autoNumeric('init',{
        aSep: '.',
        aDec: ',',
        mDec: '2'
    });
    
    $('.decimal4').autoNumeric('init',{
        aSep: '.',
        aDec: ',',
        mDec: '4'
    });

    $('.decimal5').autoNumeric('init',{
        aSep: '.',
        aDec: ',',
        mDec: '5'
    });
    
    $('.inteiro').autoNumeric('init',{
        aSep: '.',
        aDec: ',',
        mDec: '0'
    });
    
    $('.percentual0a100').autoNumeric('init',{
        aSep: '.',
        aDec: ',',
        mDec: '2',
        vMax: '100.00'
    });
}

function criaAlerta(tipo, msg, prependTo) {
    var mensagem = msg;
    console.log(typeof msg);
    
    if ( typeof msg == 'object' ){
        var span = $('<span></span>');
        var ul = $('<ul></ul>');
        $.each(msg, function(i) {
            var li = $('<li/>')
                .html(msg[i])
                .appendTo(ul);
        });
        span.append(ul);
        mensagem = span.html();
    } else if ( typeof msg == 'string' ){
        var span = $('<span></span>');
        var ul = $('<ul></ul>');
        var li = $('<li/>')
            .html(msg)
            .appendTo(ul);
        span.append(ul);
        mensagem = span.html();
        console.log(prependTo);
    }

    var classe_cor = 'danger';
    var classe_icone = 'times';
    tipo = tipo || 2;

    switch (tipo) {
        case 1:
            classe_cor = 'success';
            classe_icone = 'check';
            break;

        case 3:
            classe_cor = 'warning';
            classe_icone = 'warning';
            break;

        case 4:
            classe_cor = 'info';
            classe_icone = 'info';
            break;
    }

    var div = $('<div></div>', {
        class: 'alert alert-' + classe_cor + ' fade in',
        html: ' ' + mensagem
    });

    $('<button></button>', {
        class: 'close',
        'data-dismiss': 'alert',
        html: '&times;'
    }).prependTo(div);

    div.prepend($('<b></b>', {
        html: 'Verifique o(s) erro(s) a seguir:'
    }));

    div.prepend($('<i></i>', {
        class: 'fa-fw fa fa-' + classe_icone
    }));
    
    prependTo.find('div.alert').remove();
    
    div.prependTo(prependTo);
    
    console.log(div);
}

function limpaDados(el){
	var ss = $(el).find('select:visible, input[type=text]:visible, input[type=password]:visible');
	$.each(ss, function(i,s){
		var df = $(s).attr('data-default');
		var sv = $(s).val();
		if ( df ){
			$(s).val(df);
		} else {
			$(s).val('');
		}
	});
};