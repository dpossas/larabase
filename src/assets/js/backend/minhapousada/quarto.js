var tabelaListagem = null;

$(function(){
    pageSetUp();
    
    $('#form-filtro-'+modulo.nome).submit(function(e){
        e.preventDefault();
        
        var form = $(this);
        //muda a quantidade de paginacao antes vinda do modulo, para o que foi selecionado na listagem
        quantidade_paginacao = $('select[name="lista_'+modulo.nome+'_length"]').val();
        
        carregaLista();
        
        //escondendo o filtro após buscar - VER COM O ANDREI SE ISSO SERA PADRAO OU DEIXAREMOS O USUARIO CONFIGURAR
        form.parents('article').find('header').first().find('a.jarviswidget-toggle-btn').first().trigger('click');
    });
    
    $('body').on('click','#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input',function(e){
        e.stopPropagation();
        
        $('#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input').prop('checked',$(this).prop('checked'));
        
        $('#list-'+modulo.nome+' tbody tr td input[name^=item]').prop('checked',$(this).prop('checked'));
        
        selectTr($(this).parents('table'));
        
        //console.log(checkboxSelecionados());
    });
    
    $('body').on('click','#list-'+modulo.nome+' tbody tr',function(){
        var tr = $(this);
        var td = tr.find('td:eq(1)');
        var id = td.text();

        $(location).attr('href', '#'+urls['edit'].replace('?',id));

        return false;
    });
    
    $('body').on('click','.adicionar-arquivo',function(e){
        e.preventDefault();
        
        var id_arquivo = $(this).attr('data-id');
        
        if ( !isNaN(id_arquivo) && parseInt(id_arquivo) ){
            adicionarArquivo(id_arquivo);
        } else {
            
        }
    });
    
    $('#quarto-arquivos').on('click','a.setar-capa',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        
        $.ajax({
            type: 'POST',
            url: url,
            data: {
            },
            success: function( retorno ) {
                console.log(retorno);
            }
        }).fail(function(a,b,c){
            //divformconexao.unwrap(formconexao);
            console.log(a);
            console.log(b);
            console.log(c);
        });
    });
    
    $('#quarto-arquivos').on('click','a.excluir-arquivo',function(e){
        e.preventDefault();
        var bt = $(this);
        var url = bt.attr('href');
        
        $.confirm({
            theme: 'hololight',
            title: 'Confirmar ação!',
            content: '<b>Essa ação não poderá ser desfeita após a confirmação.</b> Tem certeza que deseja excluir esta informação?',
            confirmButton: 'Sim, tenho certeza!',
            cancelButton: 'Não',
            confirmButtonClass: 'btn-danger',
            cancelButtonClass: 'btn-default',
            backgroundDismiss: false,
            confirmKeys: [13], // ENTER key
            cancelKeys: [27], // ESC key
            confirm: function() {
                bt.parents('section.secao-arquivo').first().fadeOut();
        
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                    },
                    success: function( retorno ) {
                        //recarregarArquivos();
                    }
                }).fail(function(a,b,c){
                    //divformconexao.unwrap(formconexao);
                    console.log(a);
                    console.log(b);
                    console.log(c);
                });
            },
            cancel: function() {
                return true;
            }
        });
    });
    
    $('#quarto-mobiliario').on('submit','form#adicionar-mobiliario',function(e){
        e.preventDefault();
        var form = $(this);
        
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function( retorno ) {
                recarregarMobiliario();
            }
        }).fail(function(a,b,c){
            //divformconexao.unwrap(formconexao);
            console.log(a);
            console.log(b);
            console.log(c);
        });
    });
    
    $('#quarto-tarifario').on('submit','form#adicionar-tarifario',function(e){
        e.preventDefault();
        var form = $(this);
        
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: {
                id_tarifario: $('#id_tarifario').val(),
                valor: $('#valor').autoNumeric('get')
            },
            success: function( retorno ) {
                recarregarTarifario();
            }
        }).fail(function(a,b,c){
            //divformconexao.unwrap(formconexao);
            console.log(a);
            console.log(b);
            console.log(c);
        });
    });
    
    $('#quarto-mobiliario').on('click','a.editar-mobiliario',function(e){
        e.preventDefault();
        var bt = $(this);
        var tr = bt.parents('tr').first();
        var quantidade = tr.find('input[name=quantidade]').val();
        tr.find('.status-mobiliario').hide();
        
        $.ajax({
            type: 'POST',
            url: bt.attr('href'),
            data: {
                quantidade: quantidade
            },
            success: function( retorno ) {
                tr.find('.status-mobiliario.sucesso').fadeIn('fast').fadeOut(5000);
            },
            error: function(err){
                tr.find('.status-mobiliario.erro').fadeIn('fast').fadeOut(5000);
            }
        });
    });
    
    $('#quarto-tarifario').on('click','a.editar-tarifario',function(e){
        e.preventDefault();
        var bt = $(this);
        var tr = bt.parents('tr').first();
        var valor = tr.find('input[name=valor]').autoNumeric('get');
        tr.find('.status-tarifario').hide();
        
        $.ajax({
            type: 'POST',
            url: bt.attr('href'),
            data: {
                valor: valor
            },
            success: function( retorno ) {
                tr.find('.status-tarifario.sucesso').fadeIn('fast').fadeOut(5000);
            },
            error: function(err){
                tr.find('.status-tarifario.erro').fadeIn('fast').fadeOut(5000);
            }
        });
    });
    
    $('#quarto-mobiliario').on('click','a.excluir-mobiliario',function(e){
        e.preventDefault();
        var bt = $(this);
        var url = bt.attr('href');

        $.confirm({
            theme: 'hololight',
            title: 'Confirmar ação!',
            content: '<b>Essa ação não poderá ser desfeita após a confirmação.</b> Tem certeza que deseja excluir esta informação?',
            confirmButton: 'Sim, tenho certeza!',
            cancelButton: 'Não',
            confirmButtonClass: 'btn-danger',
            cancelButtonClass: 'btn-default',
            backgroundDismiss: false,
            confirmKeys: [13], // ENTER key
            cancelKeys: [27], // ESC key
            confirm: function() {
                bt.parents('tr.secao-mobiliario').first().fadeOut();
                
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                    },
                    success: function( retorno ) {
                        
                    }
                }).fail(function(a,b,c){
                    //divformconexao.unwrap(formconexao);
                    console.log(a);
                    console.log(b);
                    console.log(c);
                });
            },
            cancel: function() {
                return true;
            }
        });
    });
    
    $('#quarto-tarifario').on('click','a.excluir-tarifario',function(e){
        e.preventDefault();
        var bt = $(this);
        var url = bt.attr('href');

        $.confirm({
            theme: 'hololight',
            title: 'Confirmar ação!',
            content: '<b>Essa ação não poderá ser desfeita após a confirmação.</b> Tem certeza que deseja excluir esta informação?',
            confirmButton: 'Sim, tenho certeza!',
            cancelButton: 'Não',
            confirmButtonClass: 'btn-danger',
            cancelButtonClass: 'btn-default',
            backgroundDismiss: false,
            confirmKeys: [13], // ENTER key
            cancelKeys: [27], // ESC key
            confirm: function() {
                bt.parents('tr.secao-tarifario').first().fadeOut();
                
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                    },
                    success: function( retorno ) {
                        
                    }
                }).fail(function(a,b,c){
                    //divformconexao.unwrap(formconexao);
                    console.log(a);
                    console.log(b);
                    console.log(c);
                });
            },
            cancel: function() {
                return true;
            }
        });
    });
    
    carregaLista();
});

function recarregarArquivos(){
    $.ajax({
        type: 'POST',
        url: url_quartorecarregararquivo,
        data: {
        },
        dataType: 'html',
        success: function( retorno ) {
            $('#quarto-arquivos').empty().html(retorno);
        }
    }).fail(function(a,b,c){
        //divformconexao.unwrap(formconexao);
        console.log(a);
        console.log(b);
        console.log(c);
    }).done(pageSetUp);
}

function recarregarMobiliario(){
    $.ajax({
        type: 'POST',
        url: url_quartorecarregarmobiliario,
        data: {
        },
        dataType: 'html',
        success: function( retorno ) {
            $('#quarto-mobiliario').empty().html(retorno);
        }
    }).fail(function(a,b,c){
        //divformconexao.unwrap(formconexao);
        console.log(a);
        console.log(b);
        console.log(c);
    }).done(pageSetUp);
}

function recarregarTarifario(){
    $.ajax({
        type: 'POST',
        url: url_quartorecarregartarifario,
        data: {
        },
        dataType: 'html',
        success: function( retorno ) {
            $('#quarto-tarifario').empty().html(retorno);
        }
    }).fail(function(a,b,c){
        //divformconexao.unwrap(formconexao);
        console.log(a);
        console.log(b);
        console.log(c);
    }).done(pageSetUp);
}

function cbArquivo(arquivo){
    adicionarArquivo(arquivo.id);
}

function adicionarArquivo(id_arquivo){
    $.ajax({
        type: 'POST',
        url: url_quartoadicionararquivo,
        data: {
            id_arquivo: id_arquivo
        },
        success: function( retorno ) {
            recarregarArquivos();
        }
    }).fail(function(a,b,c){
        //divformconexao.unwrap(formconexao);
        console.log(a);
        console.log(b);
        console.log(c);
    });
}

function carregaLista(registros) {
    var form = $('#form-filtro-'+modulo.nome);

    tabelaListagem = $('#list-'+modulo.nome).DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": form.attr('action'),
        "sServerMethod": form.prop('method'),
        "destroy": true,
        "searching": false,
        "order": [[ 1, "desc" ]],
        "sPaginationType" : "full_numbers",
        "fnServerParams": function ( aoData ) {
            $.each( form.serializeArray(), function() {
                if ( this.value != '' )
                    aoData.push({'name':this.name, 'value':this.value});
            });
        },
        columns: [
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
            { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
            { 'data':'nome', 'title':'Descrição', 'sClass': 'left', 'bSortable': true },
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '40px', 'bSortable': false },
        ],
        columnDefs: [
            {
                "render": function ( data, type, row ) {
                    var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';

                    return links;
                },
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    var links = '<div class="btn-group" role="group" aria-label="...">';
                    
                    links += '<a href="'+urls['delete'].replace('?',data)+'" data-method="DELETE" class="btn btn-sm btn-danger" id="excluir-'+data+'" title="Excluir"><i class="fa fa-trash-o"></i> ';
                    if ( _configuracao.tipo_botao == 1 )
                        links += 'Excluir';
                    links += '</a>';

                    links += '</div>';
                    
                    return links;
                },
                "targets": 3
            },
        ],
        "oLanguage": {
            "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
        },
        fnDrawCallback: function(){
            $('div.dataTables_length').remove();
            //adiciona itens na barra superior a tabela
            var toolbar = $('#list-'+modulo.nome).parents('.dataTables_wrapper').first();
            toolbar.addClass('smart-form');
            toolbar.find('section').remove();
            
            var fieldset = $('<fieldset></fieldset>');
            var row = $('<div></div>',{
                class: 'row'
            });
            var section = $('<section></section>',{
                class: 'col col-sm-2'
            });
            var lbl = $('<label></label>',{
                for: 'name=lista_'+modulo.nome+'_length',
                html: 'Registros por Página',
                class: 'label'
            });
            
            var lblS = $('<label></label>',{
                class: 'select'
            });
            var opts = [];
            opts['20'] = 20;
            opts['40'] = 40;
            opts['60'] = 60;
            opts['80'] = 80;
            opts['100'] = 100;

            lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'name=lista_'+modulo.nome+'_length','name=lista_'+modulo.nome+'_length',opts));
            lblS.append($('<i></i>'));
            lbl.appendTo(section);
            lblS.appendTo(section);
            section.appendTo(fieldset);
            toolbar.prepend(section);
            
            var options = [];
            options[''] = '';
            options['1'] = 'Excluir';

            //toolbar.prepend(htmlSelect(null,'teste','teste',options));
            //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));

            $('#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input').removeAttr('checked');
        }
    });
}