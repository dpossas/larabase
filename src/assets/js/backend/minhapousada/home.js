var tabelaListagem = null;

$(function(){
    pageSetUp();
    
    $("#widget-grid-home").jarvisWidgets({
        "grid": "article",
        "widgets": ".jarviswidget",
        "localStorage": localStorageJarvisWidgets,
        "deleteSettingsKey": "#deletesettingskey-options",
        "settingsKeyLabel": "Reset settings?",
        "deletePositionKey": "#deletepositionkey-options",
        "positionKeyLabel": "Reset position?",
        "sortable": sortableJarvisWidgets,
        "buttonsHidden": !1,
        "toggleButton": !0,
        "toggleClass": "fa fa-angle-up | fa fa-angle-down",
        "toggleSpeed": 200,
        "onToggle": function() {},
        "deleteButton": !0,
        "deleteMsg": "Warning: This action cannot be undone!",
        "deleteClass": "fa fa-times",
        "deleteSpeed": 200,
        "onDelete": function() {},
        "editButton": !0,
        "editPlaceholder": ".jarviswidget-editbox",
        "editClass": "fa fa-cog | fa fa-save",
        "editSpeed": 200,
        "onEdit": function() {},
        "colorButton": !0,
        "fullscreenButton": !0,
        "fullscreenClass": "fa fa-expand | fa fa-compress",
        "fullscreenDiff": 3,
        "onFullscreen": function() {},
        "customButton": !1,
        "customClass": "folder-10 | next-10",
        "customStart": function() {
            alert("Hello you, this is a custom button...")
        },
        "customEnd": function() {
            alert("bye, till next time...")
        },
        "buttonOrder": "%refresh% %custom% %edit% %delete% %fullscreen% %toggle% ",
        "opacity": 1,
        "dragHandle": "> header",
        "placeholderClass": "jarviswidget-placeholder",
        "indicator": !0,
        "indicatorTime": 600,
        "ajax": !0,
        "timestampPlaceholder": ".jarviswidget-timestamp",
        "timestampFormat": "Last update: %m%/%d%/%y% %h%:%i%:%s%",
        "refreshButton": !0,
        "refreshButtonClass": "fa fa-refresh",
        "labelError": "Sorry but there was a error:",
        "labelUpdated": "Last Update:",
        "labelRefresh": "Refresh",
        "labelDelete": "Delete widget:",
        "afterLoad": function() {},
        "rtl": !1,
        "onChange": function() {},
        "onSave": function() {},
        "ajaxnav": $.navAsAjax
    });
});

function carregaLista(registros) {
    var form = $('#form-filtro-'+modulo.nome);

    tabelaListagem = $('#list-'+modulo.nome).DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": form.attr('action'),
        "sServerMethod": form.prop('method'),
        "destroy": true,
        "searching": false,
        "order": [[ 1, "desc" ]],
        "sPaginationType" : "full_numbers",
        "fnServerParams": function ( aoData ) {
            $.each( form.serializeArray(), function() {
                if ( this.value != '' )
                    aoData.push({'name':this.name, 'value':this.value});
            });
        },
        columns: [
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
            { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
            { 'data':'nome', 'title':'Nome', 'sClass': 'left', 'bSortable': true },
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
        ],
        columnDefs: [
            {
                "render": function ( data, type, row ) {
                    var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';

                    return links;
                },
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    var links = '<div class="btn-group" role="group" aria-label="...">';
                    
                    links += '<a href="'+urls['delete'].replace('?',data)+'" data-method="DELETE" class="btn btn-sm btn-danger" id="excluir-'+data+'" title="Excluir"><i class="fa fa-trash-o"></i> ';
                    if ( _configuracao.tipo_botao == 1 )
                        links += 'Excluir';
                    links += '</a>';

                    links += '</div>';
                    
                    return links;
                },
                "targets": 3
            },
        ],
        "oLanguage": {
            "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
        },
        fnDrawCallback: function(){
            $('div.dataTables_length').remove();
            //adiciona itens na barra superior a tabela
            var toolbar = $('#list-'+modulo.nome).parents('.dataTables_wrapper').first();
            toolbar.addClass('smart-form');
            toolbar.find('section').remove();
            
            var fieldset = $('<fieldset></fieldset>');
            var row = $('<div></div>',{
                class: 'row'
            });
            var section = $('<section></section>',{
                class: 'col col-sm-2'
            });
            var lbl = $('<label></label>',{
                for: 'lista_'+modulo.nome+'_length',
                html: 'Registros por Página',
                class: 'label'
            });
            
            var lblS = $('<label></label>',{
                class: 'select'
            });
            var opts = [];
            opts['20'] = 20;
            opts['40'] = 40;
            opts['60'] = 60;
            opts['80'] = 80;
            opts['100'] = 100;

            lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'lista_'+modulo.nome+'_length','lista_'+modulo.nome+'_length',opts));
            lblS.append($('<i></i>'));
            lbl.appendTo(section);
            lblS.appendTo(section);
            section.appendTo(fieldset);
            toolbar.prepend(section);
            
            var options = [];
            options[''] = '';
            options['1'] = 'Excluir';

            //toolbar.prepend(htmlSelect(null,'teste','teste',options));
            //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));

            $('#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input').removeAttr('checked');

            
        }
    });
}