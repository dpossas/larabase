var tabelaListagem = null;

$(function(){
    pageSetUp();
    
    $('#form-filtro-'+modulo.nome).submit(function(e){
        e.preventDefault();
        
        var form = $(this);
        //muda a quantidade de paginacao antes vinda do modulo, para o que foi selecionado na listagem
        quantidade_paginacao = $('select[name="lista_'+modulo.nome+'_length"]').val();
        
        carregaLista();
        
        //escondendo o filtro após buscar - VER COM O ANDREI SE ISSO SERA PADRAO OU DEIXAREMOS O USUARIO CONFIGURAR
        form.parents('article').find('header').first().find('a.jarviswidget-toggle-btn').first().trigger('click');
    });
    
    $('body').on('click','#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input',function(e){
        e.stopPropagation();
        
        $('#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input').prop('checked',$(this).prop('checked'));
        
        $('#list-'+modulo.nome+' tbody tr td input[name^=item]').prop('checked',$(this).prop('checked'));
        
        selectTr($(this).parents('table'));
        
        //console.log(checkboxSelecionados());
    });
    
    $('body').on('click','#list-'+modulo.nome+' tbody tr',function(){
        var tr = $(this);
        var td = tr.find('td:eq(1)');
        var id = td.text();
        
        $(location).attr('href', '#'+urls['edit'].replace('?',id));

        return false;
    });
    
    $("#form-cadastro").validate({
    	focusInvalid: true,
    	onkeyup: false,
    	onfocusout: false,
    	errorElement: "span",
    	errorPlacement: function(error, element) {
    		
    	},
    	invalidHandler: function(event, validator) {
    		var msgs = [];
    		$.each(validator.errorMap, function(el,i){
    			msgs.push(i);
    		});
    		
    		criaAlerta(2,msgs,$("#form-cadastro"))
    	}
    });
        
    carregaLista();
});

function carregaLista(registros) {
    var form = $('#form-filtro-'+modulo.nome);

    tabelaListagem = $('#list-'+modulo.nome).DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": form.attr('action'),
        "sServerMethod": form.prop('method'),
        "destroy": true,
        "searching": false,
        "order": [[ 1, "desc" ]],
        "sPaginationType" : "full_numbers",
        "fnServerParams": function ( aoData ) {
            $.each( form.serializeArray(), function() {
                if ( this.value != '' )
                    aoData.push({'name':this.name, 'value':this.value});
            });
        },
        columns: [
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
            { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
            { 'data':'nome_completo', 'title':'Nome Completo', 'sClass': 'left', 'bSortable': true },
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
        ],
        columnDefs: [
            {
                "render": function ( data, type, row ) {
                    var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';

                    return links;
                },
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    var links = '<div class="btn-group" role="group" aria-label="...">';
                    
                    links += '<a href="'+urls['delete'].replace('?',data)+'" data-method="DELETE" class="btn btn-sm btn-danger" id="excluir-'+data+'" title="Excluir"><i class="fa fa-trash-o"></i> ';
                    if ( _configuracao.tipo_botao == 1 )
                        links += 'Excluir';
                    links += '</a>';

                    links += '</div>';
                    
                    return links;
                },
                "targets": 3
            },
        ],
        "oLanguage": {
            "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
        },
        fnDrawCallback: function(){
            $('div.dataTables_length').remove();
            //adiciona itens na barra superior a tabela
            var toolbar = $('#list-'+modulo.nome).parents('.dataTables_wrapper').first();
            toolbar.addClass('smart-form');
            toolbar.find('section').remove();
            
            var fieldset = $('<fieldset></fieldset>');
            var row = $('<div></div>',{
                class: 'row'
            });
            var section = $('<section></section>',{
                class: 'col col-sm-2'
            });
            var lbl = $('<label></label>',{
                for: 'lista_'+modulo.nome+'_length',
                html: 'Registros por Página',
                class: 'label'
            });
            
            var lblS = $('<label></label>',{
                class: 'select'
            });
            var opts = [];
            opts['20'] = 20;
            opts['40'] = 40;
            opts['60'] = 60;
            opts['80'] = 80;
            opts['100'] = 100;

            lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'lista_'+modulo.nome+'_length','lista_'+modulo.nome+'_length',opts));
            lblS.append($('<i></i>'));
            lbl.appendTo(section);
            lblS.appendTo(section);
            section.appendTo(fieldset);
            toolbar.prepend(section);
            
            var options = [];
            options[''] = '';
            options['1'] = 'Excluir';

            //toolbar.prepend(htmlSelect(null,'teste','teste',options));
            //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));

            $('#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input').removeAttr('checked');

            
        }
    });
}