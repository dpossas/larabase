var tabelaListagemTelefone = null;
var tabelaListagemEmail = null;

var divformtelefone = $('#form-filtro-'+modulo.nome+'-telefone-div');
var formtelefone;

var divformemail = $('#form-filtro-'+modulo.nome+'-email-div');
var formemail;

$(function(){
    pageSetUp();
    
    if ( $('body').find(('#list-'+modulo.nome+'-telefone')).length > 0 ){
        formtelefone = $('<form></form>', { 'method': 'post', 'action': url_dadostelefone, 'id':'#form-filtro-'+modulo.nome+'-telefone' });
        
        $('body').on('click','#salvar-telefone',function(e){
            e.preventDefault();
            
            salvarTelefone();
        });
        
        $('body').on('click','#list-'+modulo.nome+'-telefone tbody tr',function(){
            var tr = $(this);
            var td = tr.find('td:eq(1)');
            var id = td.text();
            
            editarTelefone(id);
            
            return false;
        });
        
        $('#form-filtro-'+modulo.nome+'-telefone-submit').click(function(e){
            e.preventDefault();
            //console.log(divformtelefone);
            divformtelefone.wrap(formtelefone);
            formtelefone = divformtelefone.parent('form');
            
            quantidade_paginacao = $('select[name="lista_'+modulo.nome+'_telefone_length"]').val();
            
            carregaListaTelefone();
        });
        
        $('body').on('click','#list-'+modulo.nome+'-telefone thead tr th input, #list-'+modulo.nome+'-telefone tfoot tr th input',function(e){
            e.stopPropagation();
            
            $('#list-'+modulo.nome+'-telefone thead tr th input, #list-'+modulo.nome+'-telefone tfoot tr th input').prop('checked',$(this).prop('checked'));
            
            $('#list-'+modulo.nome+'-telefone tbody tr td input[name^=item]').prop('checked',$(this).prop('checked'));
            
            selectTr($(this).parents('table'));
            
            //console.log(checkboxSelecionados());
        });
        
        $('body').on('click','#cancelar-telefone',function(e){
            e.preventDefault();
            
            limpaDados('#form-telefone');
            $('body').find('input[name=id_telefone]:hidden').val('');
        });
        
        carregaListaTelefone();
    }
    
    if ( $('body').find(('#list-'+modulo.nome+'-email')).length > 0 ){
        formemail = $('<form></form>', { 'method': 'post', 'action': url_dadosemail, 'id':'#form-filtro-'+modulo.nome+'-email' });
        
        $('body').on('click','#salvar-email',function(e){
            e.preventDefault();
            
            salvarEmail();
        });
        
        $('body').on('click','#list-'+modulo.nome+'-email tbody tr',function(){
            var tr = $(this);
            var td = tr.find('td:eq(1)');
            var id = td.text();
            
            editarEmail(id);
            
            return false;
        });
        
        $('#form-filtro-'+modulo.nome+'-email-submit').click(function(e){
            e.preventDefault();
            //console.log(divformemail);
            divformemail.wrap(formemail);
            formemail = divformemail.parent('form');
            
            quantidade_paginacao = $('select[name="lista_'+modulo.nome+'_email_length"]').val();
            
            carregaListaEmail();
        });
        
        $('body').on('click','#list-'+modulo.nome+'-email thead tr th input, #list-'+modulo.nome+'-email tfoot tr th input',function(e){
            e.stopPropagation();
            
            $('#list-'+modulo.nome+'-email thead tr th input, #list-'+modulo.nome+'-email tfoot tr th input').prop('checked',$(this).prop('checked'));
            
            $('#list-'+modulo.nome+'-email tbody tr td input[name^=item]').prop('checked',$(this).prop('checked'));
            
            selectTr($(this).parents('table'));
            
            //console.log(checkboxSelecionados());
        });
        
        $('body').on('click','#cancelar-email',function(e){
            e.preventDefault();
            
            limpaDados('#form-email');
            $('body').find('input[name=id_email]:hidden').val('');
        });
        
        carregaListaEmail();
    }
});

function carregaListaTelefone(registros) {
    tabelaListagemTelefone = $('#list-'+modulo.nome+'-telefone').DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": formtelefone.attr('action'),
        "sServerMethod": formtelefone.prop('method'),
        "destroy": true,
        "searching": false,
        "order": [[ 1, "desc" ]],
        "sPaginationType" : "full_numbers",
        "fnServerParams": function ( aoData ) {
            $.each( formtelefone.serializeArray(), function() {
                if ( this.value != '' )
                    aoData.push({'name':this.name, 'value':this.value});
            });
        },
        columns: [
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
            { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
            { 'data':'numero', 'title':'Número', 'sClass': 'left', 'bSortable': true },
            { 'data':'principal', 'title':'Principal', 'sClass': 'left', 'bSortable': true },
            { 'data':'movel', 'title':'Móvel', 'sClass': 'left', 'bSortable': true },
            { 'data':'whatsapp', 'title':'Whatsapp', 'sClass': 'left', 'bSortable': true },
            { 'data':'operadora', 'title':'Operadora', 'sClass': 'left', 'bSortable': true },
            { 'data':'id', 'title':'', 'sClass': 'center', 'bSortable': false },
        ],
        columnDefs: [
            {
                "render": function ( data, type, row ) {
                    var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';

                    return links;
                },
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    var links = 'Não';
                    
                    if ( data == 1 )
                        links = 'Sim';
                    
                    return links;
                },
                "targets": [3,4,5]
            },
            {
                "render": function ( data, type, row ) {
                    var links = '<div class="btn-group" role="group" aria-label="...">';
                    
                    links += '<a href="/admin/minhapousada/pousada_telefone/'+data+'" data-method="DELETE" class="btn btn-sm btn-danger excluir-telefone" id="excluir-telefone-'+data+'" title="Excluir"><i class="fa fa-trash-o"></i> ';
                    if ( _configuracao.tipo_botao == 1 )
                        links += 'Excluir';
                    links += '</a>';

                    links += '</div>';
                    
                    return links;
                },
                "targets": 7
            },
        ],
        "oLanguage": {
            "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
        },
        fnDrawCallback: function(){
            $('div.dataTables_length').remove();
            //adiciona itens na barra superior a tabela
            var toolbar = $('#list-'+modulo.nome+'-telefone').parents('.dataTables_wrapper').first();
            toolbar.addClass('smart-form');
            toolbar.find('section').remove();
            
            var fieldset = $('<fieldset></fieldset>');
            var row = $('<div></div>',{
                class: 'row'
            });
            var section = $('<section></section>',{
                class: 'col col-sm-2'
            });
            var lbl = $('<label></label>',{
                for: 'name=lista_'+modulo.nome+'_telefone_length',
                html: 'Registros por Página',
                class: 'label'
            });
            
            var lblS = $('<label></label>',{
                class: 'select'
            });
            var opts = [];
            opts['20'] = 20;
            opts['40'] = 40;
            opts['60'] = 60;
            opts['80'] = 80;
            opts['100'] = 100;

            lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'name=lista_'+modulo.nome+'_telefone_length','name=lista_'+modulo.nome+'_telefone_length',opts));
            lblS.append($('<i></i>'));
            lbl.appendTo(section);
            lblS.appendTo(section);
            section.appendTo(fieldset);
            toolbar.prepend(section);
            
            var options = [];
            options[''] = '';
            options['1'] = 'Excluir';

            //toolbar.prepend(htmlSelect(null,'teste','teste',options));
            //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));

            $('#list-'+modulo.nome+'-telefone thead tr th input, #list-'+modulo.nome+'-telefone tfoot tr th input').removeAttr('checked');
        }
    });
}

function editarTelefone(row){
    var el = $('#form-telefone');
    
    if ( typeof row == 'object' ){
		for (var k in row) {
			$(el).find('*[name="telefone_'+k+'"][type!="password"]:visible').val(row[k]);
        }
		$(el).find('*[name="id_telefone"]:hidden').val(row['id']);
    } else if ( typeof row == 'string' && parseInt(row) > 0 ){
        var id = parseInt(row);
        
        $.ajax({
            type: 'GET',
            url: '/admin/minhapousada/pousada_telefone/'+id+'/editar/',
            data: {},
            success: function( retorno ) {
                el.find('input[name=id_telefone]:hidden').val(retorno.id);
                el.find('input[name=telefone_numero]').val(retorno.numero);
                el.find('select[name=telefone_principal]').val(retorno.principal);
                el.find('select[name=telefone_movel]').val(retorno.movel);
                el.find('select[name=telefone_whatsapp]').val(retorno.whatsapp);
                el.find('select[name=telefone_operadora]').val(retorno.operadora);
            }
        }).fail(function(xhr, ajaxOptions, thrownError){
            criaAlerta(2,xhr.responseJSON.err,$('#content'));
        });
    }
}

function salvarTelefone(){
    var form = $('<form class="form-temporario"></form>');
    var dform = $('#form-telefone');
    dform.wrap(form);
    var data = dform.parent('form').serialize();
    
    dform.unwrap(form);
    
    $.ajax({
        type: 'POST',
        url: url_salvartelefone,
        data: data,
        success: function( retorno ) {
            limpaDados(dform);
            $('body').find('input[name=id_telefone]:hidden').val('');
            carregaListaTelefone();
            divformtelefone.unwrap(formtelefone);
        }
    }).fail(function(a,b,c){
        divformtelefone.unwrap(formtelefone);
    });
}

function carregaListaEmail(registros) {
    tabelaListagemEmail = $('#list-'+modulo.nome+'-email').DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": formemail.attr('action'),
        "sServerMethod": formemail.prop('method'),
        "destroy": true,
        "searching": false,
        "order": [[ 1, "desc" ]],
        "sPaginationType" : "full_numbers",
        "fnServerParams": function ( aoData ) {
            $.each( formemail.serializeArray(), function() {
                if ( this.value != '' )
                    aoData.push({'name':this.name, 'value':this.value});
            });
        },
        columns: [
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
            { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
            { 'data':'email', 'title':'E-mail', 'sClass': 'left', 'bSortable': true },
            { 'data':'principal', 'title':'Principal', 'sClass': 'left', 'bSortable': true },
            { 'data':'id', 'title':'', 'sClass': 'center', 'bSortable': false },
        ],
        columnDefs: [
            {
                "render": function ( data, type, row ) {
                    var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';

                    return links;
                },
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    var links = 'Não';
                    
                    if ( data == 1 )
                        links = 'Sim';
                    
                    return links;
                },
                "targets": [3]
            },
            {
                "render": function ( data, type, row ) {
                    var links = '<div class="btn-group" role="group" aria-label="...">';
                    
                    links += '<a href="/admin/minhapousada/pousada_email/'+data+'" data-method="DELETE" class="btn btn-sm btn-danger excluir-email" id="excluir-email-'+data+'" title="Excluir"><i class="fa fa-trash-o"></i> ';
                    if ( _configuracao.tipo_botao == 1 )
                        links += 'Excluir';
                    links += '</a>';

                    links += '</div>';
                    
                    return links;
                },
                "targets": 4
            },
        ],
        "oLanguage": {
            "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
        },
        fnDrawCallback: function(){
            $('div.dataTables_length').remove();
            //adiciona itens na barra superior a tabela
            var toolbar = $('#list-'+modulo.nome+'-email').parents('.dataTables_wrapper').first();
            toolbar.addClass('smart-form');
            toolbar.find('section').remove();
            
            var fieldset = $('<fieldset></fieldset>');
            var row = $('<div></div>',{
                class: 'row'
            });
            var section = $('<section></section>',{
                class: 'col col-sm-2'
            });
            var lbl = $('<label></label>',{
                for: 'name=lista_'+modulo.nome+'_email_length',
                html: 'Registros por Página',
                class: 'label'
            });
            
            var lblS = $('<label></label>',{
                class: 'select'
            });
            var opts = [];
            opts['20'] = 20;
            opts['40'] = 40;
            opts['60'] = 60;
            opts['80'] = 80;
            opts['100'] = 100;

            lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'name=lista_'+modulo.nome+'_email_length','name=lista_'+modulo.nome+'_email_length',opts));
            lblS.append($('<i></i>'));
            lbl.appendTo(section);
            lblS.appendTo(section);
            section.appendTo(fieldset);
            toolbar.prepend(section);
            
            var options = [];
            options[''] = '';
            options['1'] = 'Excluir';

            //toolbar.prepend(htmlSelect(null,'teste','teste',options));
            //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));

            $('#list-'+modulo.nome+'-email thead tr th input, #list-'+modulo.nome+'-email tfoot tr th input').removeAttr('checked');
        }
    });
}

function editarEmail(row){
    var el = $('#form-email');
    
    if ( typeof row == 'object' ){
		for (var k in row) {
			$(el).find('*[name="email_'+k+'"][type!="password"]:visible').val(row[k]);
        }
		$(el).find('*[name="id_email"]:hidden').val(row['id']);
    } else if ( typeof row == 'string' && parseInt(row) > 0 ){
        var id = parseInt(row);
        
        $.ajax({
            type: 'GET',
            url: '/admin/minhapousada/pousada_email/'+id+'/editar/',
            data: {},
            success: function( retorno ) {
                el.find('input[name=id_email]:hidden').val(retorno.id);
                el.find('input[name=email_email]').val(retorno.email);
                el.find('select[name=email_principal]').val(retorno.principal);
            }
        }).fail(function(xhr, ajaxOptions, thrownError){
            criaAlerta(2,xhr.responseJSON.err,$('#content'));
        });
    }
}

function salvarEmail(){
    var form = $('<form class="form-temporario"></form>');
    var dform = $('#form-email');
    dform.wrap(form);
    var data = dform.parent('form').serialize();
    
    dform.unwrap(form);
    
    $.ajax({
        type: 'POST',
        url: url_salvaremail,
        data: data,
        success: function( retorno ) {
            limpaDados(dform);
            $('body').find('input[name=id_email]:hidden').val('');
            carregaListaEmail();
            divformemail.unwrap(formemail);
        }
    }).fail(function(a,b,c){
        divformemail.unwrap(formemail);
    });
}