$(function(){
	pageSetUp();
	
	$('#form-cadastro-configuracoes').submit( function(e) {
		e.preventDefault();

		var form = $(this);
		var action = form.prop('action');
		var requestType = form.prop('method');
		var datastring = form.serialize();

		$.ajax({
			type: requestType,
			url: action,
			data: datastring,
			dataType: 'json',
			success: function( item ) {
				
			}
		});
	});

	$('#skin').change(function(){
		var selected_skin = $(this).val() || 'smart-style-0';

		$('body').removeClass('smart-style-0');
		$('body').removeClass('smart-style-1');
		$('body').removeClass('smart-style-2');
		$('body').removeClass('smart-style-3');
		$('body').removeClass('smart-style-4');
		$('body').removeClass('smart-style-5');

		$('body').addClass(selected_skin);
	});
	
	$("#form-cadastro").validate({
    	focusInvalid: true,
    	onkeyup: false,
    	onfocusout: false,
    	errorElement: "span",
    	errorPlacement: function(error, element) {
    		
    	},
    	invalidHandler: function(event, validator) {
    		var msgs = [];
    		$.each(validator.errorMap, function(el,i){
    			msgs.push(i);
    		});
    		
    		criaAlerta(2,msgs,$("#form-cadastro"))
    	}
    });
	
	formataNumeros();
});