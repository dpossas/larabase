var tabelaListagem = null;
var tabelaListagemConexao = null;
var tabelaListagemRtabela = null;
var tabelaListagemRcoluna = null;
var tabelaListagemMutator = null;

var id_conexao_ativa = 0;
var id_rtabela = 0;

var divformconexao = $('#form-filtro-'+modulo.nome+'-conexao-div');
var formconexao;

var divformrtabela = $('#form-filtro-'+modulo.nome+'-rtabela-div');
var formrtabela;

var divformrcoluna = $('#form-filtro-'+modulo.nome+'-rcoluna-div');
var formrcoluna;

var divformmutator = $('#form-filtro-'+modulo.nome+'-mutator-div');
var formmutator;

$(function(){
    pageSetUp();
    
    $('#form-filtro-'+modulo.nome).submit(function(e){
        e.preventDefault();
        
        var form = $(this);
        //muda a quantidade de paginacao antes vinda do modulo, para o que foi selecionado na listagem
        quantidade_paginacao = $('select[name="lista_'+modulo.nome+'_length"]').val();
        
        carregaLista();
        
        form.parents('article').find('header').first().find('a.jarviswidget-toggle-btn').first().trigger('click');
    });
    
    $('body').on('click','#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input',function(e){
        e.stopPropagation();
        
        $('#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input').prop('checked',$(this).prop('checked'));
        
        $('#list-'+modulo.nome+' tbody tr td input[name^=item]').prop('checked',$(this).prop('checked'));
        
        selectTr($(this).parents('table'));
        
        //console.log(checkboxSelecionados());
    });
    
    
    $('body').on('click','#list-'+modulo.nome+' tbody tr',function(){
        var tr = $(this);
        var td = tr.find('td:eq(1)');
        var id = td.text();
        
        $(location).attr('href', '#/admin/'+modulo.sistema+'/'+modulo.nome+'/'+id+'/editar');

        return false;
    });
    
    carregaLista();
    
    //metodos referente a edição
    $('body').on('click','#salvar-rcoluna',function(e){
        e.preventDefault();
        
        salvarRcoluna();
    });
    
    if ( $('body').find(('#list-'+modulo.nome+'-conexao')).length > 0 ){
        formconexao = $('<form></form>', { 'method': 'post', 'action': url_dadosconexao, 'id':'#form-filtro-'+modulo.nome+'-conexao' });
        
        $('body').on('click','#salvar-conexao',function(e){
            e.preventDefault();
            
            salvarConexao();
        });
        
        $('body').on('click','#list-'+modulo.nome+'-conexao tbody tr',function(){
            var tr = $(this);
            var td = tr.find('td:eq(1)');
            var id = td.text();
            
            var d = tabelaListagemConexao.row( this ).data();
        
            editarConexao(d);
            
            return false;
        });

        //Teste de conexão
        $('body').on('click','#list-'+modulo.nome+'-conexao tbody tr td div.btn-group button.testar-conexao',function(e){
            e.preventDefault();
            e.stopPropagation();
            
            var tr = $(this).parents('tr').first();
            var td = tr.find('td:eq(1)');
            var id = td.text();
            
            testarConexao(id);
            
            return false;
        });
        
        //Teste de conexão
        $('body').on('click','#list-'+modulo.nome+'-conexao tbody tr td div.btn-group button.conectar-conexao',function(e){
            e.preventDefault();
            e.stopPropagation();
            
            var tr = $(this).parents('tr').first();
            var td = tr.find('td:eq(1)');
            var id = td.text();
            
            conectarConexao(id);
            
            return false;
        });
        
        $('#form-filtro-'+modulo.nome+'-conexao-submit').click(function(e){
            e.preventDefault();
            //console.log(divformconexao);
            divformconexao.wrap(formconexao);
            formconexao = divformconexao.parent('form');
            
            quantidade_paginacao = $('select[name="lista_'+modulo.nome+'_conexao_length"]').val();
            
            carregaListaConexao();
        });
        
        $('body').on('click','#list-'+modulo.nome+'-conexao thead tr th input, #list-'+modulo.nome+'-conexao tfoot tr th input',function(e){
            e.stopPropagation();
            
            $('#list-'+modulo.nome+'-conexao thead tr th input, #list-'+modulo.nome+'-conexao tfoot tr th input').prop('checked',$(this).prop('checked'));
            
            $('#list-'+modulo.nome+'-conexao tbody tr td input[name^=item]').prop('checked',$(this).prop('checked'));
            
            selectTr($(this).parents('table'));
            
            //console.log(checkboxSelecionados());
        });
        
        carregaListaConexao();
    }
    
    if ( $('body').find('#list-'+modulo.nome+'-mutator').length > 0 ){
        formmutator = $('<form></form>', { 'method': 'post', 'action': url_dadosmutator, 'id':'#form-filtro-'+modulo.nome+'-mutator' });
        
        $('body').on('click','#salvar-mutator',function(e){
            e.preventDefault();
            
            salvarMutator();
        });
        
        $('body').on('click','#cancelar-mutator',function(e){
            e.preventDefault();
            
            limpaDados('#form-mutator');
            $('body').find('input[name=id_mutator]:hidden').val('');
        });
        
        $('#form-filtro-'+modulo.nome+'-mutator-submit').click(function(e){
            e.preventDefault();
            //console.log(divformconexao);
            divformmutator.wrap(formconexao);
            formmutator = divformconexao.parent('form');
            
            quantidade_paginacao = $('select[name="lista_'+modulo.nome+'_mutator_length"]').val();
            
            carregaListaMutator();
        });
        
        $('body').on('click','#list-'+modulo.nome+'-mutator thead tr th input, #list-'+modulo.nome+'-mutator tfoot tr th input',function(e){
            e.stopPropagation();
            
            $('#list-'+modulo.nome+'-mutator thead tr th input, #list-'+modulo.nome+'-mutator tfoot tr th input').prop('checked',$(this).prop('checked'));
            
            $('#list-'+modulo.nome+'-mutator tbody tr td input[name^=item]').prop('checked',$(this).prop('checked'));
            
            selectTr($(this).parents('table'));
            
            //console.log(checkboxSelecionados());
        });
        
        $('body').on('click','#list-'+modulo.nome+'-mutator tbody tr',function(){
            var tr = $(this);
            var td = tr.find('td:eq(1)');
            var id = td.text();
            
            var d = tabelaListagemMutator.row( this ).data();
        
            editarMutator(d);
            
            return false;
        });
        
        carregaListaMutator();
    }
    
    if ( $('body').find('#list-'+modulo.nome+'-rtabela').length > 0 ){
        formrtabela = $('<form></form>', { 'method': 'post', 'action': url_dadosrtabela, 'id':'#form-filtro-'+modulo.nome+'-rtabela' });
        
        $('#form-filtro-'+modulo.nome+'-rtabela-submit').click(function(e){
            e.preventDefault();
            //console.log(divformconexao);
            divformrtabela.wrap(formrtabela);
            formrtabela = divformrtabeça.parent('form');
            
            quantidade_paginacao = $('select[name="lista_'+modulo.nome+'_rtabela_length"]').val();
            
            carregaListaRtabela();
        });
        
        $('body').on('click','#list-'+modulo.nome+'-rtabela thead tr th input, #list-'+modulo.nome+'-rtabela tfoot tr th input',function(e){
            e.stopPropagation();
            
            $('#list-'+modulo.nome+'-rtabela thead tr th input, #list-'+modulo.nome+'-rtabela tfoot tr th input').prop('checked',$(this).prop('checked'));
            
            $('#list-'+modulo.nome+'-rtabela tbody tr td input[name^=item]').prop('checked',$(this).prop('checked'));
            
            selectTr($(this).parents('table'));
            
            //console.log(checkboxSelecionados());
        });
        
        $('#sincronizar-rtabela').click(function(e){
            e.preventDefault();
            
            sincronizarRtabela();
        });
        
        carregaListaRtabela();
    }
    
    if ( $('body').find('#list-'+modulo.nome+'-rcoluna').length > 0 ){
        formrcoluna = $('<form></form>', { 'method': 'post', 'action': url_dadosrcoluna, 'id':'#form-filtro-'+modulo.nome+'-rcoluna' });
        
        $('#form-filtro-'+modulo.nome+'-rcoluna-submit').click(function(e){
            e.preventDefault();
            //console.log(divformconexao);
            divformrcoluna.wrap(formrcoluna);
            formrcoluna = divformrcoluna.parent('form');
            
            quantidade_paginacao = $('select[name="lista_'+modulo.nome+'_rcoluna"]').val();
            
            carregaListaRcoluna();
        });
        
        $('body').on('click','#list-'+modulo.nome+'-rcoluna thead tr th input, #list-'+modulo.nome+'-rcoluna tfoot tr th input',function(e){
            e.stopPropagation();
            
            $('#list-'+modulo.nome+'-rcoluna thead tr th input, #list-'+modulo.nome+'-rcoluna tfoot tr th input').prop('checked',$(this).prop('checked'));
            
            $('#list-'+modulo.nome+'-rcoluna tbody tr td input[name^=item]').prop('checked',$(this).prop('checked'));
            
            selectTr($(this).parents('table'));
            
            //console.log(checkboxSelecionados());
        });
        
        carregaListaRcoluna();
    }
});

function carregaLista(registros) {
    var form = $('#form-filtro-'+modulo.nome);

    tabelaListagem = $('#list-'+modulo.nome).DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": form.attr('action'),
        "sServerMethod": form.prop('method'),
        "destroy": true,
        "searching": false,
        "order": [[ 1, "desc" ]],
        "sPaginationType" : "full_numbers",
        "fnServerParams": function ( aoData ) {
            $.each( form.serializeArray(), function() {
                if ( this.value != '' )
                    aoData.push({'name':this.name, 'value':this.value});
            });
        },
        columns: [
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
            { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
            { 'data':'nome', 'title':'Nome', 'sClass': 'left', 'bSortable': true },
            { 'data':'usa_namespace', 'title':'Usa Namespaces', 'sClass': 'left', 'bSortable': true },
            { 'data':'laravel_versao', 'title':'Versão do Laravel', 'sClass': 'left', 'bSortable': true },
            { 'data':'tema_nome', 'title':'Tema', 'sClass': 'left', 'bSortable': true },
            { 'data':'tema_versao', 'title':'Versão do Tema', 'sClass': 'left', 'bSortable': true },
            { 'data':'timezone', 'title':'Timezone', 'sClass': 'left', 'bSortable': true },
            { 'data':'idioma_padrao', 'title':'Idioma Padrão', 'sClass': 'left', 'bSortable': true },
        ],
        columnDefs: [
            {
                "render": function ( data, type, row ) {
                    var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';

                    return links;
                },
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    var str = 'Não';
                    
                    if ( data == 1 )
                        str = 'Sim';
                        
                    return str;
                },
                "targets": 3
            },
            {
                "render": function ( data, type, row ) {
                    var str = 'English - EUA';
                    
                    if ( data == 'ptbr' )
                        str = 'Português Brasil';
                        
                    return str;
                },
                "targets": 8
            }
        ],
        "oLanguage": {
            "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
        },
        fnDrawCallback: function(){
            $('div.dataTables_length').remove();
            //adiciona itens na barra superior a tabela
            var toolbar = $('#list-'+modulo.nome).parents('.dataTables_wrapper').first();
            toolbar.addClass('smart-form');
            toolbar.find('section').remove();
            
            var fieldset = $('<fieldset></fieldset>');
            var row = $('<div></div>',{
                class: 'row'
            });
            var section = $('<section></section>',{
                class: 'col col-sm-2'
            });
            var lbl = $('<label></label>',{
                for: 'name=lista_'+modulo.nome+'_length',
                html: 'Registros por Página',
                class: 'label'
            });
            
            var lblS = $('<label></label>',{
                class: 'select'
            });
            var opts = [];
            opts['20'] = 20;
            opts['40'] = 40;
            opts['60'] = 60;
            opts['80'] = 80;
            opts['100'] = 100;

            lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'name=lista_'+modulo.nome+'_length','name=lista_'+modulo.nome+'_length',opts));
            lblS.append($('<i></i>'));
            lbl.appendTo(section);
            lblS.appendTo(section);
            section.appendTo(fieldset);
            toolbar.prepend(section);
            
            var options = [];
            options[''] = '';
            options['1'] = 'Excluir';

            //toolbar.prepend(htmlSelect(null,'teste','teste',options));
            //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));

            $('#list-'+modulo.nome+' thead tr th input, #list-'+modulo.nome+' tfoot tr th input').removeAttr('checked');
        }
    });
}

function carregaListaConexao(registros) {
    tabelaListagemConexao = $('#list-'+modulo.nome+'-conexao').DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": formconexao.attr('action'),
        "sServerMethod": formconexao.prop('method'),
        "destroy": true,
        "searching": false,
        "order": [[ 1, "desc" ]],
        "sPaginationType" : "full_numbers",
        "fnServerParams": function ( aoData ) {
            $.each( formconexao.serializeArray(), function() {
                if ( this.value != '' )
                    aoData.push({'name':this.name, 'value':this.value});
            });
        },
        columns: [
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
            { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
            { 'data':'nome', 'title':'Nome', 'sClass': 'left', 'bSortable': true },
            { 'data':'tipo', 'title':'Driver', 'sClass': 'left', 'bSortable': true },
            { 'data':'servidor', 'title':'Servidor', 'sClass': 'left', 'bSortable': true },
            { 'data':'banco_dados', 'title':'Nome do Banco de Dados', 'sClass': 'left', 'bSortable': true },
            { 'data':'usuario', 'title':'Usuário', 'sClass': 'left', 'bSortable': true },
            { 'data':'caracter', 'title':'Charset', 'sClass': 'left', 'bSortable': true },
            { 'data':'prefixo', 'title':'Prefixo', 'sClass': 'left', 'bSortable': true },
            { 'data':'id', 'title':'', 'sClass': 'center', 'bSortable': false },
        ],
        columnDefs: [
            {
                "render": function ( data, type, row ) {
                    var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';

                    return links;
                },
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    var links = '<div class="btn-group" role="group" aria-label="...">';
                    
                    //links += '<button class="btn btn-sm btn-default testar-conexao" title="Testar Conexão"><i class="fa fa-exchange"></i> ';
                    //if ( _configuracao.tipo_botao == 1 )
                    //    links += 'Testar';
                    //links += '</button>';
                    
                    links += '<button class="btn btn-sm btn-default conectar-conexao" id="conectar-conexao-'+data+'" title="Conectar"><i class="fa fa-circle"></i> ';
                    if ( _configuracao.tipo_botao == 1 )
                        links += 'Conectar';
                    links += '</button>';

                    links += '</div>';
                    
                    return links;
                },
                "targets": 9
            },
        ],
        "oLanguage": {
            "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
        },
        fnDrawCallback: function(){
            $('div.dataTables_length').remove();
            //adiciona itens na barra superior a tabela
            var toolbar = $('#list-'+modulo.nome+'-conexao').parents('.dataTables_wrapper').first();
            toolbar.addClass('smart-form');
            toolbar.find('section').remove();
            
            var fieldset = $('<fieldset></fieldset>');
            var row = $('<div></div>',{
                class: 'row'
            });
            var section = $('<section></section>',{
                class: 'col col-sm-2'
            });
            var lbl = $('<label></label>',{
                for: 'name=lista_'+modulo.nome+'_conexao_length',
                html: 'Registros por Página',
                class: 'label'
            });
            
            var lblS = $('<label></label>',{
                class: 'select'
            });
            var opts = [];
            opts['20'] = 20;
            opts['40'] = 40;
            opts['60'] = 60;
            opts['80'] = 80;
            opts['100'] = 100;

            lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'name=lista_'+modulo.nome+'_conexao_length','name=lista_'+modulo.nome+'_conexao_length',opts));
            lblS.append($('<i></i>'));
            lbl.appendTo(section);
            lblS.appendTo(section);
            section.appendTo(fieldset);
            toolbar.prepend(section);
            
            var options = [];
            options[''] = '';
            options['1'] = 'Excluir';

            //toolbar.prepend(htmlSelect(null,'teste','teste',options));
            //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));

            $('#list-'+modulo.nome+'-conexao thead tr th input, #list-'+modulo.nome+'-conexao tfoot tr th input').removeAttr('checked');
        }
    });
}

function carregaListaMutator(registros) {
    tabelaListagemMutator = $('#list-'+modulo.nome+'-mutator').DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": formmutator.attr('action'),
        "sServerMethod": formmutator.prop('method'),
        "destroy": true,
        "searching": false,
        "order": [[ 1, "desc" ]],
        "sPaginationType" : "full_numbers",
        "fnServerParams": function ( aoData ) {
            $.each( formmutator.serializeArray(), function() {
                if ( this.value != '' )
                    aoData.push({'name':this.name, 'value':this.value});
            });
        },
        columns: [
            { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
            { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
            { 'data':'tipo_campo', 'title':'Tipo do Campo', 'sClass': 'left', 'bSortable': true },
            { 'data':'formato_entrada', 'title':'Formato de Entrada', 'sClass': 'left', 'bSortable': true },
            { 'data':'formato_saida', 'title':'Formato de Saída', 'sClass': 'left', 'bSortable': true }
        ],
        columnDefs: [
            {
                "render": function ( data, type, row ) {
                    var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';

                    return links;
                },
                "targets": 0
            }
        ],
        "oLanguage": {
            "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
        },
        fnDrawCallback: function(){
            $('div.dataTables_length').remove();
            //adiciona itens na barra superior a tabela
            var toolbar = $('#list-'+modulo.nome+'-mutator').parents('.dataTables_wrapper').first();
            toolbar.addClass('smart-form');
            toolbar.find('section').remove();
            
            var fieldset = $('<fieldset></fieldset>');
            var row = $('<div></div>',{
                class: 'row'
            });
            var section = $('<section></section>',{
                class: 'col col-sm-2'
            });
            var lbl = $('<label></label>',{
                for: 'name=lista_'+modulo.nome+'_mutator_length',
                html: 'Registros por Página',
                class: 'label'
            });
            
            var lblS = $('<label></label>',{
                class: 'select'
            });
            var opts = [];
            opts['20'] = 20;
            opts['40'] = 40;
            opts['60'] = 60;
            opts['80'] = 80;
            opts['100'] = 100;

            lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'name=lista_'+modulo.nome+'_mutator_length','name=lista_'+modulo.nome+'_mutator_length',opts));
            lblS.append($('<i></i>'));
            lbl.appendTo(section);
            lblS.appendTo(section);
            section.appendTo(fieldset);
            toolbar.prepend(section);
            
            var options = [];
            options[''] = '';
            options['1'] = 'Excluir';

            //toolbar.prepend(htmlSelect(null,'teste','teste',options));
            //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));

            $('#list-'+modulo.nome+'-mutator thead tr th input, #list-'+modulo.nome+'-mutator tfoot tr th input').removeAttr('checked');
        }
    });
}

function carregaListaRtabela(registros) {
    if ( !isNaN(id_conexao_ativa) && parseInt(id_conexao_ativa) > 0 ){
        $('body').find('input[name=id_conexao]:hidden').val(parseInt(id_conexao_ativa));
        
        tabelaListagemRtabela = $('#list-'+modulo.nome+'-rtabela').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": formrtabela.attr('action'),
            "sServerMethod": formrtabela.prop('method'),
            "destroy": true,
            "searching": false,
            "order": [[ 1, "desc" ]],
            "sPaginationType" : "full_numbers",
            "fnServerParams": function ( aoData ) {
                aoData.push({'name': 'id_conexao', 'value': parseInt(id_conexao_ativa)});
                $.each( formrtabela.serializeArray(), function() {
                    if ( this.value != '' )
                        aoData.push({'name':this.name, 'value':this.value});
                });
            },
            columns: [
                { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
                { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
                { 'data':'nome', 'title':'Nome', 'sClass': 'left', 'bSortable': true }
            ],
            columnDefs: [
                {
                    "render": function ( data, type, row ) {
                        var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';
    
                        return links;
                    },
                    "targets": 0
                }
            ],
            "oLanguage": {
                "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
            },
            fnDrawCallback: function(){
                $('div.dataTables_length').remove();
                //adiciona itens na barra superior a tabela
                var toolbar = $('#list-'+modulo.nome+'-rtabela').parents('.dataTables_wrapper').first();
                toolbar.addClass('smart-form');
                toolbar.find('section').remove();
                
                var fieldset = $('<fieldset></fieldset>');
                var row = $('<div></div>',{
                    class: 'row'
                });
                var section = $('<section></section>',{
                    class: 'col col-sm-2'
                });
                var lbl = $('<label></label>',{
                    for: 'name=lista_'+modulo.nome+'_rtabela_length',
                    html: 'Registros por Página',
                    class: 'label'
                });
                
                var lblS = $('<label></label>',{
                    class: 'select'
                });
                var opts = [];
                opts['20'] = 20;
                opts['40'] = 40;
                opts['60'] = 60;
                opts['80'] = 80;
                opts['100'] = 100;
    
                lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'name=lista_'+modulo.nome+'_rtabela_length','name=lista_'+modulo.nome+'_rtabela_length',opts));
                lblS.append($('<i></i>'));
                lbl.appendTo(section);
                lblS.appendTo(section);
                section.appendTo(fieldset);
                toolbar.prepend(section);
                
                var options = [];
                options[''] = '';
                options['1'] = 'Excluir';
    
                //toolbar.prepend(htmlSelect(null,'teste','teste',options));
                //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));
    
                $('#list-'+modulo.nome+'-rtabela thead tr th input, #list-'+modulo.nome+'-rtabela tfoot tr th input').removeAttr('checked');
            }
        });
    }
}

function carregaListaRcoluna(registros) {
    if ( !isNaN(id_rtabela) && parseInt(id_rtabela) > 0 ){
        $('body').find('input[name=id_rtabela]:hidden').val(parseInt(id_rtabela));
        
        tabelaListagemRcoluna = $('#list-'+modulo.nome+'-rcoluna').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": formrcoluna.attr('action'),
            "sServerMethod": formrcoluna.prop('method'),
            "destroy": true,
            "searching": false,
            "order": [[ 1, "desc" ]],
            "sPaginationType" : "full_numbers",
            "fnServerParams": function ( aoData ) {
                aoData.push({'name': 'id_rtabela', 'value': parseInt(id_rtabela)});
                $.each( formrcoluna.serializeArray(), function() {
                    if ( this.value != '' )
                        aoData.push({'name':this.name, 'value':this.value});
                });
            },
            columns: [
                { 'data':'id', 'title':'', 'sClass': 'center', 'sWidth': '10px', 'bSortable': false },
                { 'data':'id', 'title':'#', 'sClass': 'right inteiro '+( _configuracao.colunaid != 0 ? 'hidden-xs' : 'hidden' ), 'sWidth': '10px', 'bSortable': true },
                { 'data':'campo', 'title':'Campo', 'sClass': 'left', 'bSortable': true }
            ],
            columnDefs: [
                {
                    "render": function ( data, type, row ) {
                        var links = '<input type="checkbox" value="'+data+'" name="item['+data+']" />';
    
                        return links;
                    },
                    "targets": 0
                }
            ],
            "oLanguage": {
                "sUrl": "/backend/js/plugin/datatables/pt-BR.json"
            },
            fnDrawCallback: function(){
                $('div.dataTables_length').remove();
                //adiciona itens na barra superior a tabela
                var toolbar = $('#list-'+modulo.nome+'-rcoluna').parents('.dataTables_wrapper').first();
                toolbar.addClass('smart-form');
                toolbar.find('section').remove();
                
                var fieldset = $('<fieldset></fieldset>');
                var row = $('<div></div>',{
                    class: 'row'
                });
                var section = $('<section></section>',{
                    class: 'col col-sm-2'
                });
                var lbl = $('<label></label>',{
                    for: 'name=lista_'+modulo.nome+'_rcoluna_length',
                    html: 'Registros por Página',
                    class: 'label'
                });
                
                var lblS = $('<label></label>',{
                    class: 'select'
                });
                var opts = [];
                opts['20'] = 20;
                opts['40'] = 40;
                opts['60'] = 60;
                opts['80'] = 80;
                opts['100'] = 100;
    
                lblS.append(htmlSelect({selectedValue: registros || modulo.paginacao_registros, class: 'recarrega'},'name=lista_'+modulo.nome+'_rcoluna_length','name=lista_'+modulo.nome+'_rcoluna_length',opts));
                lblS.append($('<i></i>'));
                lbl.appendTo(section);
                lblS.appendTo(section);
                section.appendTo(fieldset);
                toolbar.prepend(section);
                
                var options = [];
                options[''] = '';
                options['1'] = 'Excluir';
    
                //toolbar.prepend(htmlSelect(null,'teste','teste',options));
                //toolbar.prepend(htmlLabel("Com selecionados:",'teste'));
    
                $('#list-'+modulo.nome+'-rcoluna thead tr th input, #list-'+modulo.nome+'-rcoluna tfoot tr th input').removeAttr('checked');
            }
        });
    }
}

function testarConexao(row){
    if ( typeof row == 'string' && parseInt(row) > 0 ){
        var id = row.id;
        
        $.ajax({
            type: 'POST',
            url: url_testarconexao,
            data: {
                'conexao_id': parseInt(row)
            },
            success: function( retorno ) {
                console.log(retorno);
            }
        }).fail(function(a,b,c){
            //divformconexao.unwrap(formconexao);
            console.log(a);
            console.log(b);
            console.log(c);
        });
    }
}

function conectarConexao(row){
    if ( typeof row == 'string' && parseInt(row) > 0 ){
        var id = parseInt(row);
        
        $.ajax({
            type: 'POST',
            url: url_conectarconexao,
            data: {
                'conexao_id': id
            },
            dataType: 'json',
            success: function( retorno ) {
                $('button[id^="conectar-conexao-"] i').removeClass('text-success');
                id_conexao_ativa = 0;
                $('body').find('*[name="id_conexao"]:hidden').val(0);
                
                if ( retorno.conn == true ){
                    $('button[id="conectar-conexao-'+id+'"] i').addClass('text-success')
                    $('body').find('*[name="id_conexao"]:hidden').val(id);
                    $('#li-edita-tabelas a').trigger('click');
                    id_conexao_ativa = id;
                    
                    carregaListaRtabela();
                }
            }
        }).fail(function(xhr, ajaxOptions, thrownError){
            $('button[id="conectar-conexao-'+id+'"] i').addClass('text-danger');
            
            criaAlerta(2,xhr.responseJSON.err,$('#content'));
        });
    }
}

function editarMutator(row){
    if ( typeof row == 'object' ){
		var el = $('#form-mutator');
        for (var k in row) {
			$(el).find('*[name="mutator_'+k+'"][type!="password"]:visible').val(row[k]);
        }
		$(el).find('*[name="id_mutator"]:hidden').val(row['id']);
    }
}

function editarConexao(row){
    if ( typeof row == 'object' ){
		var el = $('#form-conexao');
        for (var k in row) {
			$(el).find('*[name="conexao_'+k+'"][type!="password"]:visible').val(row[k]);
        }
		$(el).find('*[name="id_conexao"]:hidden').val(row['id']);
    }
}

function salvarConexao(){
    var form = $('<form class="form-temporario"></form>');
    var dform = $('#form-conexao');
    dform.wrap(form);
    var data = dform.parent('form').serialize();
    dform.unwrap(form);
    
    $.ajax({
        type: 'POST',
        url: url_salvarconexao,
        data: data,
        success: function( retorno ) {
            limpaDados(dform);
            carregaListaConexao();
            divformconexao.unwrap(formconexao);
        }
    }).fail(function(a,b,c){
        divformconexao.unwrap(formconexao);
    });
}

function salvarMutator(){
    var form = $('<form class="form-temporario"></form>');
    var dform = $('#form-mutator');
    dform.wrap(form);
    var data = dform.parent('form').serialize();
    dform.unwrap(form);
    
    $.ajax({
        type: 'POST',
        url: url_salvarmutator,
        data: data,
        success: function( retorno ) {
            limpaDados(dform);
            carregaListaMutator();
            divformmutator.unwrap(formmutator);
        }
    }).fail(function(a,b,c){
        divformmutator.unwrap(formmutator);
    });
}

function sincronizarRtabela(){
    if ( parseInt(id_conexao_ativa) > 0 ){
        $.ajax({
            type: 'POST',
            url: url_sincronizarrtabela,
            data: {
                'conexao_id': id_conexao_ativa
            },
            dataType: 'json',
            success: function( retorno ) {
                if ( retorno.conn == true ){
                    //tratar
                }
            }
        }).fail(function(xhr, ajaxOptions, thrownError){
            //tratar
        });
    }
}

function limpaDados(el){
	var ss = $(el).find('select:visible, input[type=text]:visible, input[type=password]:visible');
	$.each(ss, function(i,s){
		var df = $(s).attr('data-default');
		var sv = $(s).val();
		if ( df ){
			$(s).val(df);
		} else {
			$(s).val('');
		}
	});
};