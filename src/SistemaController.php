<?php

namespace App\Http\Controllers\Basel5;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use \Cartalyst\Sentry\Facades\Laravel\Sentry;
use \App\Models\Basel5\User;

class SistemaController extends Controller
{
    use \ForbiddenTrait;

    private $modulo = null;

    public function __construct(){
        //buscando o sistema basel5
        $base = \App\Models\Basel5\Sistema::where('nome','=','basel5')->first();

        $this->modulo = \App\Models\Basel5\Modulo::where('nome','=','sistema')
            ->where('sistema_id','=',$base->id)->firstOrFail();
    }
    
    public static function rotas()
    {
        return [
            ['method'=>'post','name'=>'data', 'alias'=>'dados']
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_acesso') ) ){
            return view('backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index');
        } else {
            return $this->acessoNegado();
        }
    }

    public function data(Request $request)
    {
        if ( $request->get('t') === 'select2' ) {
            $result = Collection::make(DB::table('tabela')
                            ->select('id', 'titulo', 'nome')
                            ->where('nome','like','%'.$request->get('q').'%')
                            ->where('titulo','like','%'.$request->get('q').'%')
                            ->where('descricao','like','%'.$request->get('q').'%')
                            ->orderBy('titulo','asc')
                            ->get())->keyBy('id');

            echo json_encode($result);

            return;
        }

        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_acesso') ) ){
            $fields = array(
                'id',
                'nome',
                'titulo',
                'descricao'
                );
            $all = \App\Models\Basel5\Sistema::select($fields);

            if ( $request->get('nome') )
                $all->where('nome','like','%'.$request->get('nome').'%');

            if ( $request->get('titulo') )
                $all->where('titulo','like','%'.$request->get('titulo').'%');

            if ( $request->get('descricao') )
                $all->where('descricao','like','%'.$request->get('descricao').'%');

            return \Datatables::of($all)->make(true);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_criacao') ) ){
            $listasistemas = \App\Models\Basel5\Sistema::orderBy('nome')->get();
            return view('backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')
                ->with(['listasistemas'=>$listasistemas]);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_criacao') ) ){
            $rules = array(
                'titulo'=>'required',
                'nome'=>'required',
                'prefixo_tabela'=>'required',
                'ativo'=>'required|boolean',
                'system'=>'required|boolean'
            );

            $validator = \Validator::make($request->all(), $rules);

            if ( $validator->fails() ){
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')),[],false))
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {
                    $item = new \App\Models\Basel5\Sistema;
                    $item->titulo = $request->get('titulo');
                    $item->nome = $request->get('nome');
                    $item->descricao = $request->get('descricao');
                    $item->prefixo_tabela = $request->get('prefixo_tabela');
                    $item->ativo = $request->get('ativo');
                    $item->system = $request->get('system');
                    $item->user_create_id = Sentry::getUser() && Sentry::getUser()->id ? Sentry::getUser()->id : User::firstOrFail()->id;
                    $item->user_update_id = Sentry::getUser() && Sentry::getUser()->id ? Sentry::getUser()->id : User::firstOrFail()->id;

                    if ( !$item->save() ) { 
                        return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')),[],false))
                            ->withErrors([trans('message.saveerror')])
                            ->withInput() ;
                    } else {
                        $request->session()->flash('success', trans('messages.savesuccess'));

                        return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false));
                    }
                } catch ( Exception $e ){
                    return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')),[],false))
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //acao não permitida
        return $this->acessoNegado();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $item = \App\Models\Basel5\Sistema::find($id);
            $listasistemas = \App\Models\Basel5\Sistema::orderBy('nome')->get();

            return view( 'backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit')
                ->with([
                    'item'=>$item,
                    'listasistemas'=>$listasistemas
                ]);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $rules = array(
                'titulo'=>'required',
                'nome'=>'required',
                'prefixo_tabela'=>'required',
                'ativo'=>'required|boolean',
                'system'=>'required|boolean'
            );

            $validator = \Validator::make($request->all(), $rules);
            
            if ( $validator->fails() ){
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit',$id)),[],false))
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {
                    $item = \App\Models\Basel5\Sistema::find($id);
                    $item->titulo = $request->get('titulo');
                    $item->nome = $request->get('nome');
                    $item->descricao = $request->get('descricao');
                    $item->prefixo_tabela = $request->get('prefixo_tabela');
                    $item->ativo = $request->get('ativo');
                    $item->system = $request->get('system');
                    $item->user_update_id = Sentry::getUser() && Sentry::getUser()->id ? Sentry::getUser()->id : User::firstOrFail()->id;
                    
                    if ( !$item->save() ) { 
                        return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit',$id)),[],false))
                            ->withErrors([trans('messages.saveerror')])
                            ->withInput() ;
                    } else {
                        $request->session()->flash('success', trans('messages.savesuccess'));

                        return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false));
                    }
                } catch ( Exception $e ){
                    return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit',$id)),[],false))
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_exclusao') ) ){
            $item = \App\Models\Basel5\Sistema::find($id);
            
            if ( !$item ){
                $request->session()->flash('error', trans('messages.notfound'));
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false))
                        ->withInput($request->except('password'));
            } else {
                if ( !$item->delete() ){
                    $request->session()->flash('error', trans('messages.deleteerror'));
                } else {
                    $request->session()->flash('success', trans('messages.deletesuccess'));
                }
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false));
            }
        } else {
            return $this->acessoNegado();
        }
    }
}
