<?php

namespace App\Http\Controllers\Basel5;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use \Cartalyst\Sentry\Facades\Laravel\Sentry;
use \App\Models\Basel5\User;
use Intervention\Image\ImageManagerStatic as Image;

class ArquivoController extends Controller
{
    use \ForbiddenTrait;
    
    private $system = 'basel5';
    private $module = 'arquivo';
    private $modulo;
    private $model = '\App\\Models\\Basel5\\Arquivo';
    private $uploaded_file;

    public function __construct(){
        //buscando o sistema basel5
        $base = \App\Models\Basel5\Sistema::where('nome','=',$this->system)->first();

        $this->modulo = \App\Models\Basel5\Modulo::where('nome','=',$this->module)
            ->where('sistema_id','=',$base->id)->firstOrFail();
    }
    
    public static function rotas()
    {
        return [
            ['method'=>'post','name'=>'data', 'alias'=>'dados']
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->module.'_acesso') ) ){
            $moduloArquivo = $this->modulo;
            
            return view(strtolower(implode('.',['backend',$this->system,$this->module,'index'])))
            ->with(compact('moduloArquivo'));
        } else {
            return $this->acessoNegado();
        }
    }

    public function data(Request $request)
    {
        if ( $request->get('t') === 'select2' ) {
            $result = Collection::make(DB::table('tabela')
                            ->select('id', 'descricao')
                            ->where('descricao','like','%'.$request->get('q').'%')
                            ->orderBy('descricao','asc')
                            ->get())->keyBy('id');

            echo json_encode($result);

            return;
        }

        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->module.'_acesso') ) ){
            $fields = array(
                'id',
                'descricao'
                );
                
            $all = call_user_func([$this->model,'select'],$fields);

            if ( $request->get('descricao') )
                $all->where('descricao','like','%'.$request->get('descricao').'%');

            return \Datatables::of($all)->make(true);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->module.'_criacao') ) ){
            return view(strtolower(implode('.',['backend',$this->system,$this->module,'create'])));
        } else {
            return $this->acessoNegado();
        }
    }
    
    public function isImage($mime)
    {
        if( substr($mime, 0, 5) == 'image') {
            return true;
        }
        
        return false;
    }
    
    public function uploadImage()
    {
        try {
            $pathimage = public_path('upload/img');
            \File::makeDirectory($pathimage, 0775, true, true);
            
            $original_name = pathinfo($this->uploaded_file->getClientOriginalName(), PATHINFO_FILENAME);
            $original_ext = $this->uploaded_file->getClientOriginalExtension();
            $original_size = $this->uploaded_file->getClientSize();
            $mimetype = $this->uploaded_file->getMimeType();
            
            $contador = 1;
            do {
                $slugname = str_slug($original_name.'-'.$contador,'-');
                $filename = $slugname.'.'.$original_ext;
                $contador++;
            } while ( \File::exists($pathimage.'/'.$filename) );
            
            $this->uploaded_file->move($pathimage, $filename);
            $image = Image::make($pathimage.'/'.$filename);
            $imageFit = Image::make($pathimage.'/'.$filename);;
            $imageFit->fit(120,120)->save($pathimage.'/thumb-'.$filename);
            
            $item = new $this->model;
            $item->nome_original = $original_name;
            $item->slug = $filename;
            $item->titulo = $original_name;
            $item->texto_alternativo = $filename;
            $item->descricao = null;
            $item->mimetype = $mimetype;
            $item->extensao_original = $original_ext;
            $item->tamanho_byte = $original_size;
            $item->imagem_largura = $image->width();
            $item->imagem_altura = $image->height();

            $item->user_create_id = Sentry::getUser()->id;
            $item->user_update_id = Sentry::getUser()->id;
            
            $item->save();
            
            return response($item, 200);
        } catch (Exception $e) {
            return response(trans('error.generic',$e->getMessage()), 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->uploaded_file = $request->file('file');
        
        if ( $this->isImage($this->uploaded_file->getMimeType()) ){
            return $this->uploadImage();
        }
        
        return "nao";
        
        dd($this->uploaded_file);
        return response(123, 200);
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->module.'_criacao') ) ){
            $rules = array(
                'descricao'=>'required'
            );

            $validator = \Validator::make($request->all(), $rules);

            if ( $validator->fails() ){
                return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/criar')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {

                    $item = new $this->model;
                    $item->descricao = $request->get('descricao');

                    if ( !$item->save() ) { 
                        return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/criar')
                            ->withErrors([trans('message.saveerror')])
                            ->withInput() ;
                    } else {
                        $request->session()->flash('success', trans('messages.savesuccess'));

                        return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome);
                    }
                } catch ( Exception $e ){
                    return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/criar')
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->module.'_edicao') ) ){
            $itens = call_user_func([$this->model,'all']);
            $tipoarquivos = \DB::table('basel5_arquivo')
                ->select(\DB::raw('count(*) as total, extensao_original'))
                ->groupBy('extensao_original')
                ->get();

            return view(strtolower(implode('.',['backend',$this->system,$this->module,'edit'])))
                ->with(compact(['itens','tipoarquivos']));
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->module.'_edicao') ) ){
            $rules = array(
                'descricao'=>'required'
            );

            $validator = \Validator::make($request->all(), $rules);
            
            if ( $validator->fails() ){
                return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/'.$id.'/editar')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {
                    $item = call_user_func([$this->model,'find'],$id);
                    $item->descricao = $request->get('descricao');
                    
                    if ( !$item->save() ) { 
                        return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/'.$id.'/editar')
                            ->withErrors([trans('messages.saveerror')])
                            ->withInput() ;
                    } else {
                        $request->session()->flash('success', trans('messages.savesuccess'));

                        return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome);
                    }
                } catch ( Exception $e ){
                    return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/'.$id.'/editar')
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->module.'_exclusao') ) ){
            $item = call_user_func([$this->model,'find'],$id);
            
            if ( !$item ){
                $request->session()->flash('error', trans('messages.notfound'));
                return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome)
                        ->withInput($request->except('password'));
            } else {
                if ( !$item->delete() ){
                    $request->session()->flash('error', trans('messages.deleteerror'));
                } else {
                    $request->session()->flash('success', trans('messages.deletesuccess'));
                }
                return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome);
            }
        } else {
            return $this->acessoNegado();
        }
    }
}
