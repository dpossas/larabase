<?php

namespace App\Http\Controllers\Basel5;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use \Cartalyst\Sentry\Facades\Laravel\Sentry;
use \App\Models\Basel5\User;

class UsuarioController extends Controller
{
    use \ForbiddenTrait;

    private $modulo = null;

    public function __construct(){
        //buscando o sistema basel5
        $base = \App\Models\Basel5\Sistema::where('nome','=','basel5')->first();

        $this->modulo = \App\Models\Basel5\Modulo::where('nome','=','usuario')
            ->where('sistema_id','=',$base->id)->firstOrFail();
    }
    
    public static function rotas()
    {
        return [
            ['method'=>'post','name'=>'data', 'alias'=>'dados']
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_acesso') ) ){
            return view('backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index');
        } else {
            return $this->acessoNegado();
        }
    }

    public function data()
    {
        if ( \Input::has('t') && \Input::get('t') === 'select2' ) {
            $result = Collection::make(DB::table('tabela')
                            ->select('id', 'first_name', 'last_name', 'email')
                            ->where('email','like','%'.\Input::get('q').'%')
                            ->orderBy('email','asc')
                            ->get())->keyBy('id');

            echo json_encode($result);

            return;
        }

        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_acesso') ) ){
            $fields = array(
                'id',
                'first_name',
                'last_name',
                'email'
                );
            $all = User::select($fields);

            if ( \Input::get('first_name') )
                $all->where('first_name','like','%'.\Input::get('first_name').'%');

            if ( \Input::get('last_name') )
                $all->where('last_name','like','%'.\Input::get('last_name').'%');

            if ( \Input::get('email') )
                $all->where('email','like','%'.\Input::get('email').'%');

            return \Datatables::of($all)->make(true);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_criacao') ) ){
            $groups = Sentry::findAllGroups();

            return view('backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')
                ->with([
                        'groups' => $groups
                    ]);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_criacao') ) ){
            $rules = array(
                'first_name'=>'required',
                'last_name'=>'required',
                'email'=>'required'
            );

            $validator = \Validator::make($request->all(), $rules);

            if ( $validator->fails() ){
                return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/criar')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {
                    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                    $password = substr( str_shuffle( $chars ), 0, 8 );

                    $params = [
                        'email' => $request->get('email'),
                        'password' => $password,
                        'permissions' => array(),
                        'activated' => true,
                        'activation_code' => '',
                        'activated_at' => date('Y-m-d h:i:s'),
                        'last_login' => null,
                        'persist_code' => null,
                        'reset_password_code' => null,
                        'first_name' => $request->get('first_name'),
                        'last_name' => $request->get('last_name'),
                    ];

                    $user = Sentry::createUser($params);

                    if ( $request->has('permissoes') )
                        foreach ( $request->get('permissoes') as $gid){
                            $group = Sentry::findGroupById($gid);
                            $user->addGroup($group);
                        }

                    $request->session()->flash('success', trans('messages.savesuccess'));
                    return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome);
                } catch ( Exception $e ){
                    return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/criar')
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //acao não permitida
        return $this->acessoNegado();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $item = Sentry::findUserById($id);
            $groups = Sentry::findAllGroups();

            return view( 'backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit')
                ->with([
                    'item'=>$item,
                    'groups'=>$groups
                ]);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $rules = array(
                'email'=>'required'
            );

            $validator = \Validator::make($request->all(), $rules);
            
            if ( $validator->fails() ){
                return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/'.$id.'/editar')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {
                    $item = \Sentry::findUserById($id);

                    $item->email = $request->get('email');
                    $item->first_name = $request->get('first_name');
                    $item->last_name = $request->get('last_name');

                    $item->save();

                    $userGroups = $item->groups()->lists('group_id')->all();
                    $selectedGroups = $request->get('permissoes', array());
                    $groupsToAdd    = array_diff($selectedGroups, $userGroups);
                    $groupsToRemove = array_diff($userGroups, $selectedGroups);
                    
                    if ( $request->has('permissoes') )
                        foreach ( $groupsToAdd as $gid){
                            $group = Sentry::findGroupById($gid);
                            $item->addGroup($group);
                        }

                    foreach ( $groupsToRemove as $gid){
                        $group = Sentry::findGroupById($gid);
                        $item->removeGroup($group);
                    }
                    
                    $request->session()->flash('success', trans('messages.savesuccess'));
                    return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome);
                } catch ( Exception $e ){
                    return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome.'/'.$id.'/editar')
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_exclusao') ) ){
            $item = User::find($id);
            
            if ( !$item ){
                $request->session()->flash('error', trans('messages.notfound'));
                return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome)
                        ->withInput(\Input::except('password'));
            } else {
                if ( !$item->delete() ){
                    $request->session()->flash('error', trans('messages.deleteerror'));
                } else {
                    $request->session()->flash('success', trans('messages.deletesuccess'));
                }
                return redirect('/admin/#/admin/'.$this->modulo->sistema()->nome.'/'.$this->modulo->nome);
            }
        } else {
            return $this->acessoNegado();
        }
    }
}
