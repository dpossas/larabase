<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title>{!! Config::get('app.title') !!}</title>
		<meta name="description" content="{!! Config::get('app.description') !!}">
		<meta name="author" content="{!! Config::get('app.author') !!}">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		{!! Html::style('/backend/css/bootstrap.min.css', array('media'=>'screen')) !!}
		{!! Html::style('/backend/css/font-awesome.min.css', array('media'=>'screen')) !!}
		{!! Html::style('/backend/css/select2.min.css') !!}
		
		{!! Html::style('/backend/css/smartadmin-production-plugins.min.css', array('media'=>'screen')) !!}
		{!! Html::style('/backend/css/smartadmin-production.min.css', array('media'=>'screen')) !!}
		{!! Html::style('/backend/css/smartadmin-skins.min.css', array('media'=>'screen')) !!}
		
		{!! Html::style('/backend/css/smartadmin-rtl.min.css', array('media'=>'screen')) !!} 
		
		{!! Html::style('/backend/css/demo.min.css', array('media'=>'screen')) !!} 

		{!! Html::style('/backend/css/app.css', array('media'=>'screen')) !!}
		{!! Html::style('/backend/css/custom.css', array('media'=>'screen')) !!}

		<!-- #FAVICONS -->
		<link rel="shortcut icon" href="{!! asset('/assets/images/favicon.ico') !!}" type="image/x-icon">
		<link rel="icon" href="{!! asset('/assets/images/favicon.ico') !!}" type="image/x-icon">
		<!-- #GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
	</head>
	
	<body class="animated fadeInDown">

		<header id="header">

		    <div id="logo-group">
		        <span id="logo"> <img src="{!! URL::to('/backend/img/logo.png') !!}" alt="SmartAdmin" class=""> 
		    </div>

		    <span id="extr-page-header-space"></span>

		</header>

		<div id="main" role="main">

		    <!-- MAIN CONTENT -->
		    <div id="content" class="container">

		        <div class="row">
		            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
		                <h1 class="txt-color-red login-header-big">BASE L5</h1>
		                <div class="hero">
		                    <div class="pull-left login-desc-box-l">
		                        <h4 class="paragraph-header">Sistema Base desenvolvido em Laravel 5. Autor: Douglas Bezerra Possas - douglaspossas@gmail.com</h4>
		                    </div>
		                </div>

		                <div class="row">
		                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		                        <h5 class="about-heading"></h5>
		                        <p></p>
		                    </div>
		                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		                        <h5 class="about-heading"></h5>
		                        <p></p>
		                    </div>
		                </div>

		            </div>
		            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
		                <div class="well no-padding">
		                    {!! Form::open(array('route'=>'adm.login','method'=>'POST', 'class'=>'smart-form client-form', 'id'=>'login-form')) !!}
		                        <header>
		                            Acessar
		                        </header>

		                        <fieldset>
									@if ( session()->has('loginerror') )
									<div class="alert alert-danger">
										<button class="close" data-dismiss="alert">
											×
										</button>
										<i class="fa-fw fa fa-times"></i>
										<strong>Erro!</strong> {!! session('loginerror') !!}
									</div>
									@endif
		                            <section>
		                                <label class="label">E-mail</label>
		                                <label class="input"> <i class="icon-append fa fa-user"></i>
		                                    <input type="email" name="email" value="{!! Input::old('email') !!}" autofocus>
		                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Este campo e obrigatorio</b></label>
		                            </section>

		                            <section>
		                                <label class="label">Senha</label>
		                                <label class="input"> <i class="icon-append fa fa-lock"></i>
		                                    <input type="password" name="password" autofocus>
		                                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Este campo e obrigatorio</b> </label>
		                                <div class="note hidden">
		                                    <a href="forgotpassword.html">Perdeu sua senha?</a>
		                                </div>
		                            </section>

		                            <section>
		                                <label class="checkbox">
		                                    <input type="checkbox" name="remember" checked="">
		                                    <i></i>Manter-se logado</label>
		                            </section>
		                        </fieldset>
		                        <footer>
		                            <button type="submit" class="btn btn-primary">
		                                Acessar
		                            </button>
		                        </footer>
		                    {!! Form::close() !!}

		                </div>

		            </div>
		        </div>
		    </div>

		</div>
		<!-- END SHORTCUT AREA -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		{!! Html::script('/backend/js/plugin/pace/pace.min.js',array('data-pace-options'=>'{ "restartOnRequestAfter": true }')) !!}
		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		{!! Html::script('//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js') !!}
		<script> if (!window.jQuery) { document.write('<script src="/backend/js/libs/jquery-2.1.1.min.js"><\/script>');} </script>
		
		{!! Html::script('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js') !!}
		<script> if (!window.jQuery.ui) { document.write('<script src="/backend/js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>
		<!-- IMPORTANT: APP CONFIG -->
		{!! Html::script('/backend/js/app.config.js') !!}

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

		<!-- BOOTSTRAP JS -->		
		{!! Html::script('/backend/js/bootstrap/bootstrap.min.js') !!}

		<!-- JQUERY VALIDATE -->
		{!! Html::script('/backend/js/plugin/jquery-validate/jquery.validate.min.js') !!}
		
		<!-- JQUERY MASKED INPUT -->
		{!! Html::script('/backend/js/plugin/masked-input/jquery.maskedinput.min.js') !!}
		
		<!--[if IE 8]>
			
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
			
		<![endif]-->

		<!-- MAIN APP JS FILE -->
		{!! Html::script('/backend/js/app.min.js') !!}
		<script type="text/javascript">
			runAllForms();

			$(function() {
				// setup widgets
	 			setup_widgets_desktop();

				pageSetUp();

				@yield('bottomjsonload')
			});
		</script>

	</body>
</html>