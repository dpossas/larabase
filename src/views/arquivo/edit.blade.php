<fieldset class="smart-form">
	<div class="row padding-10">
		<ul class="nav nav-pills" id="filters" role="tablist">
			<li role="presentation" class="active">
				<a href="#" data-filter="*">Todos <span class="badge">{!! $itens->count() !!}</span></a>
			</li>
			@foreach ( $tipoarquivos as $k => $t )
			<li role="presentation">
				<a href="#" data-filter="{!! $t->extensao_original !!}">
					*.{!! $t->extensao_original !!} 
					<span class="badge">{!! $t->total !!}</span>
				</a>
			</li>
			@endforeach
		</ul>
	</div>
	<div class="row padding-10">
		<section class="col col-xs-12 col-sm-8 col-md-9">
			@foreach ( $itens as $k => $i )
			<section class="col col-xs-12 col-sm-4 col-md-3">
				<section class="col col-xs-12">
					<a href="#" data-id="{!! $i->id !!}" class="adicionar-arquivo">
						<img class="img-responsive arquivo-selecao {!! $i->extensao_original !!}" src="{!! '/upload/img/thumb-'.$i->slug !!}" alt="{!! $i->texto_alternativo !!}">
					</a>
				</section>
				<section class="col col-xs-12">
					<div class="btn-group btn-group-xs btn-group-justified" role="group" aria-label="...">
						<a href="#" title="{!! trans('lbl.editar') !!}" class="btn btn-sm btn-default editar-arquivo" data-id="{!! $i->id !!}">
							<i class="fa fa-edit"></i> 
						</a>
						<a href="#" title="{!! trans('lbl.excluir') !!}" class="btn btn-sm btn-danger excluir-arquivo" data-id="{!! $i->id !!}">
							<i class="fa fa-trash-o"></i> 
						</a>
					</div>
				</section>
			</section>
			@endforeach
		</section>
		<section class="col col-xs-12 col-sm-4 col-md-3">
		</section>
	</div>
</fieldset>