<div class="modal fade modal-arquivos" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">{!! trans('lbl.galeriadearquivos') !!}</h4>
			</div>
			<div class="modal-body">
				<section id="widget-grid-arquivo">
			        <div class="row">
			            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			                <div class="jarviswidget" id="wid-id-arquivos"
			                	data-widget-colorbutton="false"	
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="false" 
								data-widget-sortable="false">
						        <header>
						            <ul id="widget-tab-arquivos" class="nav nav-tabs">
										<li class="active">
											<a data-toggle="tab" href="#upload"> 
												<i class="fa fa-lg fa-arrow-circle-o-up"></i> 
												<span class=""> {!! trans('lbl.envirarquivo') !!} </span> 
											</a>
										</li>
										<li>
											<a data-toggle="tab" href="#arquivos"> 
												<i class="fa fa-lg fa-arrow-circle-o-down"></i> 
												<span class=""> {!! trans('lbl.arquivos') !!} </span>
											</a>
										</li>
									</ul>
						        </header>
						        <div role="content">
						            <div class="jarviswidget-editbox">
						                <input class="form-control" type="text">
						            </div>
						            <div class="widget-body no-padding">
										<div class="tab-content">
											<div class="tab-pane fade in active" id="upload">
												{!! Form::open(array('url' => array('/admin/basel5/arquivo') , 'method' => 'POST', 'class'=>'smart-form dropzone','novalidate', 'files' => true, "id"=>"dpzonearquivo")) !!}
													<div class="fallback">
														<fieldset>
															<div class="row">
																<section class="col col-sm-12">
																	{!! Form::label('arquivo',trans('lbl.arquivo'),array('class'=>'label')) !!}
																	<label class="input"> 
																		{!! Form::text('arquivo',Input::old('arquivo'),array('id'=>'arquivo','required')) !!}
																	</label>
																</section>
															</div>
														</fieldset>
													</div>
												{!! Form::close() !!}
						                	</div>
						                	<div class="tab-pane fade in" id="arquivos">
						                		{!! App::make('\App\Http\Controllers\Basel5\ArquivoController')->edit(1) !!}
						                	</div>
					                	</div>
						            </div>
						        </div>
						    </div>
			
			            </article>
			        </div>
			    </section>
		    </div>
		</div>
	</div>
</div>

@section('jsbottom')
<script>
	modulo = {!! json_encode($moduloArquivo) !!};
	var _urlarquivos = '/admin/basel5/arquivo/1/editar';
</script>
{!! Html::script('/backend/js/backend/'.$moduloArquivo->sistema()->nome.'/'.$moduloArquivo->nome.'.js') !!}
@stop