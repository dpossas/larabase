@extends('layout.backend.main')

@section('content')

<section id="widget-grid" class="">
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget" id="wid-id-0"
				data-widget-colorbutton="false"	
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="false" 
				data-widget-sortable="false">
				
				<header>
					<span class="widget-icon"> 
						<i class="fa fa-{!! $_modulo->icone !!}"></i> 
					</span>
					<h2>{!! $_modulo->titulo !!}</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox">
						<input class="form-control" type="text">	
					</div>
					<div class="widget-body no-padding">
						{!! Form::open(array('url'=>route(strtolower(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'store'])),[],false), 'method' => 'POST', 'class'=>'smart-form','novalidate')) !!}
							<fieldset>
								<div class="row">
									<section class="col col-sm-12">
										{!! Form::label('name',trans('lbl.nome'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('name',Input::old('name'),array('id'=>'name','required')) !!}
										</label>
									</section>
								</div>
							</fieldset>
							<header>{!! trans('lbl.permissoes') !!}</header>
							<fieldset>
								<div class="row">
									<section class="col col-sm-12">
										<ul id="sistemas" class="nav nav-tabs bordered">
										@foreach ( $listasistemas as $k => $sistema )
											<li @if ($k == 0) class="active"@endif>
												<a href="#sistema-{!! $sistema->id !!}" data-toggle="tab">
													{!! $sistema->titulo !!}
												</a>
											</li>	
										@endforeach
										</ul>
										<div id="sistemaTabContent" class="tab-content">
											@foreach ( $listasistemas as $k => $sistema )
												<div class="tab-pane fade @if ( $k == 0 ) in @endif active" id="sistema-{!! $sistema->id !!}">
													<div class="panel-group smart-accordion-default" id="accordion-">
														@foreach ( $sistema->modulos() as $k => $modulof )
															@if ( !$modulof->modulo_id )
																<div class="panel panel-default">
																	<div class="panel-heading">
																		<h4 class="panel-title">
																			<a data-toggle="collapse" data-parent="#accordion-{!! $modulof->id !!}" href="#modulof-{!! $modulof->id !!}" class="panel-collapse collapse in">
																				<i class="fa fa-lg fa-angle-down pull-right"></i>
																				<i class="fa fa-lg fa-angle-up pull-right"></i>
																				{!! $modulof->titulo !!} 
																			</a>
																		</h4>
																	</div>
																</div>

																@if ( $modulof->modulos()->count() )
																	<div id="modulof-{!! $modulof->id !!}" class="panel-collapse collapse @if ( $k == 0 ) in @endif">
																		<div class="panel-body no-padding">
																			<table width="100%" class="table table-condensed">
																				<tr>
										                                            <td width="40%">&nbsp;&nbsp;&nbsp;<strong>{!! trans('lbl.modulo') !!}</strong></td>
										                                            <td width="15%"><label class="checkbox"><input type="checkbox" name="acesso[]" value="{!! $modulof->nome !!}_acesso"><i></i>&nbsp;{!! trans('lbl.acessar') !!}</label></td>
										                                            <td width="15%">&nbsp;</td>
										                                            <td width="15%">&nbsp;</td>
										                                            <td width="15%">&nbsp;</td>
										                                        </tr>
																				@foreach ( $modulof->modulos() as $mk => $mod )
																					@if ( $mod->tipo == 'ME' || $mod->tipo == 'SE' || $mod->tipo == 'CI' )	

																					@elseif ( $mod->tipo == 'CS' )
																						<tr>
															                                <td width="40%">{!! $mod->titulo !!}</td>
															                                <td width="15%"><label class="checkbox"><input type="checkbox" name="acesso[]" value="{!! $mod->nome !!}_acesso"><i></i>&nbsp;{!! trans('lbl.acessar') !!}</label></td>
															                                <td width="15%">&nbsp;</td>
															                                <td width="15%"><label class="checkbox"><input type="checkbox" name="edicao[]" value="{!! $mod->nome !!}_edicao"><i></i>&nbsp;{!! trans('lbl.gravar') !!}</label></td>
															                                <td width="15%">&nbsp;</td>
															                            </tr>
																					@elseif ( $mod->tipo == 'CC' )
																						<tr>
															                                <td width="40%">{!! $mod->titulo !!}</td>
															                                <td width="15%"><label class="checkbox"><input type="checkbox" name="acesso[]" value="{!! $mod->nome !!}_acesso"><i></i>&nbsp;{!! trans('lbl.acessar') !!}</label></td>
															                                <td width="15%"><label class="checkbox"><input type="checkbox" name="inclusao[]" value="{!! $mod->nome !!}_inclusao"><i></i>&nbsp;{!! trans('lbl.incluir') !!}</label></td>
															                                <td width="15%"><label class="checkbox"><input type="checkbox" name="edicao[]" value="{!! $mod->nome !!}_edicao"><i></i>&nbsp;{!! trans('lbl.editar') !!}</label></td>
															                                <td width="15%"><label class="checkbox"><input type="checkbox" name="exclusao[]" value="{!! $mod->nome !!}_exclusao"><i></i>&nbsp;{!! trans('lbl.excluir') !!}</label></td>
															                            </tr>
																					@endif
																				@endforeach
																			</table>
																		</div>
																	</div>
																@endif
															@endif
														@endforeach
													</div>
												</div>
											@endforeach
										</div>
									</section>
								</div>
							</fieldset>
							<footer>
								<button class="btn btn-primary" title="{!! trans('btn.salvar') !!}"><i class="fa fa-save"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.salvar') !!}@endif</button>
								<a href="#{!! URL::route(strtolower(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'index'])),[],false) !!}" title="{!! trans('btn.voltar') !!}" class="btn bg-color-blueLight txt-color-white">
									<i class="fa fa-arrow-left"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.adicionar') !!}@endif
								</a>
							</footer>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</article>
	</div>
	<div class="row">
		<div class="col-sm-12">
		</div>
	</div>
</section>

@stop

<?php
	if ( !Request::ajax() ){
?>
@section('jsbottom')
<?php
}
?>
	<script>
		modulo = {!! json_encode($_modulo) !!};
	</script>
	{!! Html::script('/backend/js/backend/'.$_modulo->sistema.'/'.$_modulo->nome.'.js') !!}
<?php
	if ( !Request::ajax() ){
?>
@stop
<?php
}
?>