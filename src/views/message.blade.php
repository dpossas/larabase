@extends('layout.backend.main')

@section('content')

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<div class="row">
			<div class="col-sm-12">
				<div class="text-center error-box">
					<h1 class="error-text tada animated">
						<i class="fa fa-lock text-danger error-icon-shadow"></i> 
						Erro 403
					</h1>
					<h2 class="font-xl"><strong>Oooops, você não pode acessar este conteúdo!</strong></h2>
					<br>
					<p class="lead semi-bold">
						<strong>
							Este conteúdo está bloqueado para você.
						</strong>
						<br/><br/>
						<small>
							Se existir realmente a necessidade de acessar este conteúdo, por favor entre em contato com o administrador do seu sistema e solicite a liberação de acesso. Abaixo estão alguns administradores que poderão lhe ajudar.
						</small>
					</p>
					<ul class="error-search text-left font-md">
			            <li>NOME DO ADM - EMAIL DO ADM</a></li>
			        </ul>
				</div>

			</div>

		</div>

	</div>
</div>

@stop