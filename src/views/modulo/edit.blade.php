@extends('layout.backend.main')

@section('content')

<section id="widget-grid" class="">
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
			<div class="jarviswidget" id="wid-id-0"
				data-widget-colorbutton="false"	
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="false" 
				data-widget-sortable="false">
				
				<header>
					<span class="widget-icon"> 
						<i class="fa fa-{!! $_modulo->icone !!}"></i> 
					</span>
					<h2>{!! $_modulo->titulo !!}</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox">
						<input class="form-control" type="text">	
					</div>
					<div class="widget-body no-padding">
						{!! Form::model($item, array('url' => route(strtolower(camel_case(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'update']))),[$item->id],false), 'method' => 'POST', 'class'=>'smart-form','novalidate')) !!}
							<fieldset>
								<div class="row">
									<section class="col col-sm-5">
										{!! Form::label('sistema_id',trans('lbl.sistema'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('sistema_id',$listasistemas,Input::old('sistema_id'),array('required')) !!}
											<i></i>
										</label>
									</section>
									<section class="col col-sm-5">
										{!! Form::label('modulo_id',trans('lbl.modulo'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('modulo_id',$listamodulos,Input::old('modulo_id'),array('required')) !!}
											<i></i>
										</label>
									</section>
									<section class="col col-sm-2">
										{!! Form::label('icone',trans('lbl.icone'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('icone',Input::old('icone'),array('id'=>'icone','required')) !!}
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-4">
										{!! Form::label('titulo',trans('lbl.titulo'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('titulo',Input::old('titulo'),array('id'=>'titulo','required')) !!}
										</label>
									</section>
									<section class="col col-sm-4">
										{!! Form::label('nome',trans('lbl.nome'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('nome',Input::old('nome'),array('id'=>'nome','required')) !!}
										</label>
									</section>
									<section class="col col-sm-4">
										{!! Form::label('tabela',trans('lbl.tabela'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('tabela',Input::old('tabela'),array('id'=>'tabela','required')) !!}
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-12">
										{!! Form::label('descricao',trans('lbl.descricao'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::textarea('descricao',Input::old('descricao'),array('id'=>'descricao','class'=>'form-control','size'=>'30x5','required')) !!}
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-6">
										{!! Form::label('tipo',trans('lbl.tipo'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('tipo',$tipo_opcoes,Input::old('tipo'),array('required')) !!}
											<i></i>
										</label>
									</section>
									<section class="col col-sm-3">
										{!! Form::label('ativo',trans('lbl.ativo'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('ativo',$_simnao_opcoes,Input::old('ativo'),array('required')) !!}
											<i></i>
										</label>
									</section>
									<section class="col col-sm-3">
										{!! Form::label('menu',trans('lbl.exibemenu'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('menu',$_simnao_opcoes,Input::old('menu'),array('required')) !!}
											<i></i>
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-3">
										{!! Form::label('customizado',trans('lbl.customizado'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('customizado',$_simnao_opcoes,Input::old('customizado'),array('required')) !!}
											<i></i>
										</label>
									</section>
									<section class="col col-sm-9">
										{!! Form::label('registro_id',trans('lbl.registro'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('registro_id',Input::old('registro_id'),array('id'=>'registro_id','required')) !!}
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-12">
										{!! Form::label('destino',trans('lbl.destino'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('destino',Input::old('destino'),array('id'=>'destino','required')) !!}
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-2">
										{!! Form::label('janela_largura',trans('lbl.janela_largura'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('janela_largura',Input::old('janela_largura'),array('id'=>'janela_largura','class'=>'inteiro')) !!}
										</label>
									</section>
									<section class="col col-sm-2">
										{!! Form::label('janela_altura',trans('lbl.janela_altura'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('janela_altura',Input::old('janela_altura'),array('id'=>'janela_altura','class'=>'inteiro')) !!}
										</label>
									</section>
									<section class="col col-sm-3">
										{!! Form::label('paginacao',trans('lbl.paginacao'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('paginacao',$_simnao_opcoes,Input::old('paginacao'),array('required')) !!}
											<i></i>
										</label>
									</section>
									<section class="col col-sm-2">
										{!! Form::label('paginacao_registros',trans('lbl.paginacao_registros'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('paginacao_registros',Input::old('paginacao_registros'),array('id'=>'paginacao_registros','class'=>'inteiro')) !!}
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-12">
										{!! Form::label('modulo_clonagem_id',trans('lbl.modulo_clonagem'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('modulo_clonagem_id',[],Input::old('modulo_clonagem_id'),array()) !!}
											<i></i>
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-12">
										{!! Form::label('modulo_substituido_id',trans('lbl.modulo_substituido'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('modulo_substituido_id',[],Input::old('modulo_substituido_id'),array()) !!}
											<i></i>
										</label>
									</section>
								</div>
							</fieldset>
							<footer>
								<button class="btn btn-primary" title="{!! trans('btn.salvar') !!}"><i class="fa fa-save"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.salvar') !!}@endif</button>
								<a href="#{!! URL::route(strtolower(camel_case(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'index']))),[],false) !!}" title="{!! trans('btn.voltar') !!}" class="btn bg-color-blueLight txt-color-white">
									<i class="fa fa-arrow-left"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.voltar') !!}@endif
								</a>
							</footer>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</article>
		
		<article class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
			<div class="jarviswidget" id="wid-id-modulos-orden"
				data-widget-colorbutton="false"	
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="false" 
				data-widget-sortable="false">
				
				<header>
					<span class="widget-icon"> 
						<i class="fa fa-sort"></i> 
					</span>
					<h2>{!! trans('lbl.modulos') !!}</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox">
						<input class="form-control" type="text">	
					</div>
					<div class="widget-body no-padding">
						{!! Form::model($item, array('url' => route(strtolower(camel_case(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'update']))),[$item->id],false), 'method' => 'POST', 'class'=>'smart-form','novalidate')) !!}
							<fieldset>
								<div class="row">
									<section class="col col-sm-12">
										@if ( $item->modulos()->count() > 0 )
										<div class="dd" id="modulos">
											<ol class="dd-list">
												@foreach ( $item->modulos()->where('modulo_id',$item->id)->orderBy('ordem','asc')->get() as $k => $i )
													<li class="dd-item dd3-item" data-id="{!! $i->id !!}">
														<div class="dd-handle dd3-handle" style="padding: 8px 2px !important;">.</div>
														<div class="dd3-content">
															{!! $i->titulo.' ('.$i->modulos()->count().')' !!}
															@if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($_modulo->nome.'_edicao') ) )
															<div class="pull-right">
																<a href="#{!! route('adm.basel5.modulo.edit',$i->id,false) !!}">
																	<i class="fa fa-edit"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.editar') !!}@endif
																</a>
															</div>
															@endif
														</div>
													</li>
												@endforeach
											</ol>
										</div>
										@else
										<center>{!! trans('lbl.semregistro') !!}</center>
										@endif
									</section>
								</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</article>
	</div>
	<div class="row">
		<div class="col-sm-12">
		</div>
	</div>
</section>

@stop

<?php
	if ( !Request::ajax() ){
?>
@section('jsbottom')
<?php
}
?>
	<script>
		modulo = {!! json_encode($_modulo) !!};
		var urls = [];
		urls['index'] = '{!! route(strtolower(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'index'])),[],false) !!}';
		urls['edit'] = '{!! route(strtolower(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'edit'])),['?'],false) !!}';
		urls['modulos'] = '{!! route(strtolower(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'modulos'])),[],false) !!}';
		var urlordenarmodulos = '{!! route('adm.basel5.modulo.ordenarmodulos') !!}';
	</script>
	{!! Html::script('/backend/js/plugin/jquery-nestable/jquery.nestable.min.js') !!}
	{!! Html::script('/backend/js/backend/'.$_modulo->sistema.'/'.$_modulo->nome.'.js') !!}
<?php
	if ( !Request::ajax() ){
?>
@stop
<?php
}
?>