@extends('layout.backend.main')

@section('content')

<section id="widget-grid" class="">
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget" id="wid-id-0"
				data-widget-colorbutton="false"	
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="false" 
				data-widget-sortable="false">
				
				<header>
					<span class="widget-icon"> 
						<i class="fa fa-{!! $_modulo->icone !!}"></i> 
					</span>
					<h2>{!! $_modulo->titulo !!}</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox">
						<input class="form-control" type="text">	
					</div>
					<div class="widget-body no-padding">
						{!! Form::open(array('url'=>route(strtolower(camel_case(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'store']))),[],false), 'method' => 'POST', 'class'=>'smart-form','novalidate','id'=>'form-cadastro')) !!}
							<fieldset>
								<div class="row">
									<section class="col col-sm-6">
										{!! Form::label('titulo',trans('lbl.titulo'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('titulo',old('titulo'),[
												'id'=>'titulo',
												'data-rule-required'=>'true',
												'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.titulo')])
											]) !!}
										</label>
									</section>
									<section class="col col-sm-6">
										{!! Form::label('nome',trans('lbl.nome'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('nome',old('nome'),[
												'id'=>'nome',
												'data-rule-required'=>'true',
												'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.nome')])
											]) !!}
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-12">
										{!! Form::label('descricao',trans('lbl.descricao'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::textarea('descricao',old('descricao'),[
												'id'=>'descricao',
												'class'=>'form-control',
												'size'=>'30x5'
											]) !!}
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-sm-6">
										{!! Form::label('prefixo_tabela',trans('lbl.prefixo_tabela'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('prefixo_tabela',old('prefixo_tabela'),[
												'id'=>'prefixo_tabela',
												'data-rule-required'=>'true',
												'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.prefixo_tabela')])
											]) !!}
										</label>
									</section>
									<section class="col col-sm-3">
										{!! Form::label('ativo',trans('lbl.ativo'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('ativo',$_simnao_opcoes,old('ativo'),[
												'data-rule-required'=>'true',
												'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.ativo')])
											]) !!}
											<i></i>
										</label>
									</section>
									<section class="col col-sm-3">
										{!! Form::label('system',trans('lbl.superusuario'),array('class'=>'label')) !!}
										<label class="select">
											{!! Form::select('system',$_simnao_opcoes,old('system'),[
												'data-rule-required'=>'true',
												'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.superusuario')])
											]) !!}
											<i></i>
										</label>
									</section>
								</div>
							</fieldset>
							<footer>
								<button class="btn btn-primary" title="{!! trans('btn.salvar') !!}"><i class="fa fa-save"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.salvar') !!}@endif</button>
								<a href="#{!! URL::route(strtolower(camel_case(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'index']))),[],false) !!}" title="{!! trans('btn.voltar') !!}" class="btn bg-color-blueLight txt-color-white">
									<i class="fa fa-arrow-left"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.voltar') !!}@endif
								</a>
							</footer>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</article>
	</div>
	<div class="row">
		<div class="col-sm-12">
		</div>
	</div>
</section>

@stop

<?php
	if ( !Request::ajax() ){
?>
@section('jsbottom')
<?php
}
?>
	<script>
		modulo = {!! json_encode($_modulo) !!};
	</script>
	{!! Html::script('/backend/js/plugin/jquery-nestable/jquery.nestable.min.js') !!}
	{!! Html::script('/backend/js/backend/'.$_modulo->sistema.'/'.$_modulo->nome.'.js') !!}
<?php
	if ( !Request::ajax() ){
?>
@stop
<?php
}
?>