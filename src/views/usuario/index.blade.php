@extends('layout.backend.main')

@section('content')

<section id="widget-grid" class="">
	<div class="row">
        <fieldset>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget" id="filtro-{!! $_modulo->nome !!}"
                    data-widget-collapsed="true"
                    data-widget-colorbutton="false"	
                    data-widget-editbutton="false"
                    data-widget-deletebutton="false"
                    data-widget-fullscreenbutton="false"
                    data-widget-custombutton="false"
                    data-widget-sortable="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>{!! trans('lbl.filtro') !!}</h2>
                    </header>
                    <div style="display: none;">
                        <div class="widget-body  no-padding">
                        	{!! Form::open(array('route'=>$_modulo->nome.'.dados', 'method'=>'POST', 'class'=>'smart-form','novalidate','id' => 'form-filtro-'.$_modulo->nome)) !!}
	                        	<fieldset>
									<div class="row">
										<section class="col col-sm-4">
											{!! Form::label('first_name',trans('lbl.nome'),array('class'=>'label')) !!}
											<label class="input"> 
												{!! Form::text('first_name',Input::old('first_name'),array('type'=>'text','id'=>'first_name')) !!}
											</label>
										</section>
										<section class="col col-sm-4">
											{!! Form::label('last_name',trans('lbl.sobrenome'),array('class'=>'label')) !!}
											<label class="input"> 
												{!! Form::text('last_name',Input::old('last_name'),array('type'=>'text','id'=>'last_name')) !!}
											</label>
										</section>
										<section class="col col-sm-4">
											{!! Form::label('email',trans('lbl.email'),array('class'=>'label')) !!}
											<label class="input"> 
												{!! Form::text('email',Input::old('email'),array('type'=>'text','id'=>'email')) !!}
											</label>
										</section>
									</div>
								</fieldset>
								<footer>
	                                <button class="btn btn-primary" title="{!! trans('btn.filtrar') !!}">
	                                	<i class="fa fa-search"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.filtrar') !!}@endif
	                                </button>
                            	</footer>
							{!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </article>

        </fieldset>
    </div>

	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget" id="wid-id-0"
				data-widget-colorbutton="false"	
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="false" 
				data-widget-sortable="false">
				
				<header>
					<span class="widget-icon"> 
						<i class="fa fa-{!! $_modulo->icone !!}"></i> 
					</span>
					<h2>{!! $_modulo->titulo !!} </h2>
					<div class="widget-toolbar">
						@if ( 1 == 1 || Sentry::getUser() && (Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($_modulo->nome.'_criacao') ) )
						<a href="{!! URL::to('#/admin/'.$_modulo->sistema.'/'.$_modulo->nome.'/criar') !!}" title="{!! trans('btn.adicionar') !!}" class="btn btn-primary" role="menu">
							<i class="fa fa-plus"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.adicionar') !!}@endif
						</a>
						@endif
					</div>
				</header>
				<div>
					<div class="jarviswidget-editbox">
						<input class="form-control" type="text">	
					</div>
					<div class="widget-body">
						<fieldset>
							<div class="row">
								<section class="col col-sm-12">
									<div class="table-responsive">
										<table class="table table-striped table-bordered" id="list-{!! $_modulo->nome !!}">
											<thead>
			                                    <tr>
			                                        <th align="center"><input type="checkbox" name="selecionaTodos" value="S" /></th>
			                                        <th>{!! trans('lbl.codigo') !!}</th>
			                                        <th>{!! trans('lbl.nome') !!}</th>
			                                        <th>{!! trans('lbl.sobrenome') !!}</th>
			                                        <th>{!! trans('lbl.email') !!}</th>
			                                    </tr>
			                                </thead>
			                                <tfoot>
			                                    <tr>
			                                        <th align="center"><input type="checkbox" name="selecionaTodos" value="S" /></th>
			                                        <th>{!! trans('lbl.codigo') !!}</th>
			                                        <th>{!! trans('lbl.nome') !!}</th>
			                                        <th>{!! trans('lbl.sobrenome') !!}</th>
			                                        <th>{!! trans('lbl.email') !!}</th>
			                                    </tr>
			                                </tfoot>
										</table>
									</table>
								</section>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
	<div class="row">
		<div class="col-sm-12">
		</div>
	</div>
</section>

@stop

<?php
	if ( !Request::ajax() ){
?>
@section('jsbottom')
<?php
}
?>
	<script>
		modulo = {!! json_encode($_modulo) !!};
	</script>
	{!! Html::script('/backend/js/backend/'.$_modulo->sistema.'/'.$_modulo->nome.'.js') !!}
<?php
	if ( !Request::ajax() ){
?>
@stop
<?php
}
?>