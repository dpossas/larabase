@extends('layout.backend.main')

@section('content')

<section id="widget-grid" class="">
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget" id="wid-id-0"
				data-widget-colorbutton="false"	
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="false" 
				data-widget-sortable="false">
				
				<header>
					<span class="widget-icon"> 
						<i class="fa fa-{!! $_modulo->icone !!}"></i> 
					</span>
					<h2>{!! $_modulo->titulo !!}</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox">
						<input class="form-control" type="text">	
					</div>
					<div class="widget-body no-padding">
						{!! Form::model($item, array('url' => array('/admin/'.$_modulo->sistema.'/'.$_modulo->nome, $item->id) , 'method' => 'POST', 'class'=>'smart-form','novalidate')) !!}
							<fieldset>
								<div class="row">
									<section class="col col-sm-4">
										{!! Form::label('first_name',trans('lbl.nome'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('first_name',Input::old('first_name'),array('id'=>'first_name','required')) !!}
										</label>
									</section>
									<section class="col col-sm-4">
										{!! Form::label('last_name',trans('lbl.sobrenome'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('last_name',Input::old('last_name'),array('id'=>'last_name','required')) !!}
										</label>
									</section>
									<section class="col col-sm-4">
										{!! Form::label('email',trans('lbl.email'),array('class'=>'label')) !!}
										<label class="input"> 
											{!! Form::text('email',Input::old('email'),array('id'=>'email','required')) !!}
										</label>
									</section>
								</div>
							</fieldset>
							<header>{!! trans('lbl.permissoes') !!}</header>
							<fieldset>
								<div class="row">
									@foreach ( $groups as $g )
									<section class="col col-sm-4">
										<label class="checkbox">
											<input type="checkbox" name="permissoes[]" @if ( $item->inGroup($g) ) checked @endif value="{!! $g->id !!}"><i></i>&nbsp;{!! $g->name !!}
										</label>
									</section>
									@endforeach
								</div>
							</fieldset>
							<footer>
								<button class="btn btn-primary" title="{!! trans('btn.salvar') !!}"><i class="fa fa-save"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.salvar') !!}@endif</button>
								<a href="{!! URL::to('/admin/#/admin/'.$_modulo->sistema.'/'.$_modulo->nome) !!}" title="{!! trans('btn.voltar') !!}" class="btn bg-color-blueLight txt-color-white">
									<i class="fa fa-arrow-left"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.adicionar') !!}@endif
								</a>
							</footer>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</article>
	</div>
	<div class="row">
		<div class="col-sm-12">
		</div>
	</div>
</section>

@stop

<?php
	if ( !Request::ajax() ){
?>
@section('jsbottom')
<?php
}
?>
	<script>
		modulo = {!! json_encode($_modulo) !!};
	</script>
	{!! Html::script('/backend/js/backend/'.$_modulo->sistema.'/'.$_modulo->nome.'.js') !!}
<?php
	if ( !Request::ajax() ){
?>
@stop
<?php
}
?>