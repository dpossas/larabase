@extends('layout.backend.main')

@section('content')

<section id="widget-grid" class="">
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget" id="wid-id-0"
				data-widget-colorbutton="false"	
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="false" 
				data-widget-sortable="false">
				
				<header>
					<span class="widget-icon"> 
						<i class="fa fa-{!! $_modulo->icone !!}"></i> 
					</span>
					<h2>{!! $_modulo->titulo !!}</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox">
						<input class="form-control" type="text">	
					</div>
					<div class="widget-body no-padding">
						{!! Form::model($item, array('url' => route(strtolower(camel_case(implode('.',['adm',$_modulo->sistema,$_modulo->nome,'update']))),[$item->id],false), 'method' => 'POST', 'class'=>'smart-form','novalidate','id'=>'form-cadastro')) !!}
							<fieldset>
								<div class="row">
									<section class="col col-sm-12">
										<ul id="sistemas" class="nav nav-tabs bordered">
											<li class="active">
												<a href="#informacao" data-toggle="tab">
													{!! trans('lbl.informacao') !!}
												</a>
											</li>
											<li>
												<a href="#aparencia" data-toggle="tab">
													{!! trans('lbl.aparencia') !!}
												</a>
											</li>
											<li>
												<a href="#email" data-toggle="tab">
													{!! trans('lbl.email') !!}
												</a>
											</li>
										</ul>
										<div id="sistemaTabContent" class="tab-content">
											<div class="tab-pane fade in active" id="informacao">
												<fieldset>
													<div class="row">
														<section class="col col-sm-10">
															{!! Form::label('titulo',trans('lbl.titulo'),array('class'=>'label')) !!}
															<label class="input"> 
																{!! Form::text('titulo',old('titulo'),[
																	'id'=>'titulo',
																	'data-rule-required'=>'true',
																	'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.titulo')])
																]) !!}
															</label>
														</section>
														<section class="col col-sm-2">
															{!! Form::label('versao',trans('lbl.versao'),array('class'=>'label')) !!}
															<label class="input"> 
																{!! Form::text('versao',old('versao'),[
																	'id'=>'versao',
																	'data-rule-required'=>'true',
																	'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.versao')])
																]) !!}
															</label>
														</section>
													</div>
												</fieldset>
											</div>
											<div class="tab-pane fade in" id="aparencia">
												<fieldset>
													<div class="row">
														<section class="col col-sm-2">
															{!! Form::label('skin',trans('lbl.skin'),array('class'=>'label')) !!}
															<label class="select">
																{!! Form::select('skin',$skins_opcoes,old('skins_opcoes'),[
																	'data-rule-required'=>'true',
																	'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.skin')])
																]) !!}
																<i></i>
															</label>
														</section>
														<section class="col col-sm-2">
															{!! Form::label('paginacao_registros',trans('lbl.paginacao_registros'),array('class'=>'label')) !!}
															<label class="input"> 
																{!! Form::text('paginacao_registros',old('paginacao_registros'),[
																	'id'=>'paginacao_registros',
																	'class'=>'inteiro',
																	'data-rule-required'=>'true',
																	'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.paginacao_registros')])
																]) !!}
															</label>
														</section>
														<section class="col col-sm-2">
															{!! Form::label('tipo_botao',trans('lbl.tipo_botao'),array('class'=>'label')) !!}
															<label class="select">
																{!! Form::select('tipo_botao',$botoes_opcoes,old('tipo_botao'),[
																	'data-rule-required'=>'true',
																	'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.tipo_botao')])
																]) !!}
																<i></i>
															</label>
														</section>
														<section class="col col-sm-2">
															{!! Form::label('colunaid',trans('lbl.colunaid'),array('class'=>'label')) !!}
															<label class="select">
																{!! Form::select('colunaid',$colunaid_opcoes,old('colunaid'),[
																	'data-rule-required'=>'true',
																	'data-msg-required'=>trans('validation.required',['attribute'=>trans('lbl.colunaid')])
																]) !!}
																<i></i>
															</label>
														</section>
													</div>
												</fieldset>
											</div>
											<div class="tab-pane fade in" id="email">
												<fieldset>
													<div class="row">
														<section class="col col-sm-3">
															{!! Form::label('smtp_servidor',trans('lbl.smtp_servidor'),array('class'=>'label')) !!}
															<label class="input"> 
																{!! Form::text('smtp_servidor',old('smtp_servidor'),[
																	'id'=>'smtp_servidor',
																	'required'
																]) !!}
															</label>
														</section>
														<section class="col col-sm-1">
															{!! Form::label('smtp_porta',trans('lbl.smtp_porta'),array('class'=>'label')) !!}
															<label class="input"> 
																{!! Form::text('smtp_porta',old('smtp_porta'),[
																	'id'=>'smtp_porta',
																	'required'
																]) !!}
															</label>
														</section>
														<section class="col col-sm-2">
															{!! Form::label('smtp_auth_type',trans('lbl.smtp_auth_type'),array('class'=>'label')) !!}
															<label class="select">
																{!! Form::select('smtp_auth_type',[null=>trans('lbl.selecione')]+$smtpauth_opcoes,old('smtp_auth_type'),[
																	'required'
																]) !!}
																<i></i>
															</label>
														</section>
														<section class="col col-sm-2">
															{!! Form::label('smtp_timeout',trans('lbl.smtp_timeout'),array('class'=>'label')) !!}
															<label class="input"> 
																{!! Form::text('smtp_timeout',old('smtp_timeout'),[
																	'id'=>'smtp_timeout',
																	'class'=>'inteiro',
																	'required'
																]) !!}
															</label>
														</section>
														<section class="col col-sm-2">
															{!! Form::label('smtp_usuario',trans('lbl.smtp_usuario'),array('class'=>'label')) !!}
															<label class="input"> 
																{!! Form::text('smtp_usuario',old('smtp_usuario'),[
																	'id'=>'smtp_usuario',
																	'required'
																]) !!}
															</label>
														</section>
														<section class="col col-sm-2">
															{!! Form::label('smtp_senha',trans('lbl.smtp_senha'),array('class'=>'label')) !!}
															<label class="input"> 
																{!! Form::password('smtp_senha',[
																	'id'=>'smtp_senha',
																	'required'
																]) !!}
															</label>
														</section>
													</div>
												</fieldset>
											</div>
										</div>
									</section>
								</div>
							</fieldset>
							<footer>
								<button class="btn btn-primary" title="{!! trans('btn.salvar') !!}"><i class="fa fa-save"></i>@if ( $_configuracao->tipo_botao )&nbsp;&nbsp;{!! trans('btn.salvar') !!}@endif</button>
							</footer>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</article>
	</div>
	<div class="row">
		<div class="col-sm-12">
		</div>
	</div>
</section>

@stop

<?php
	if ( !Request::ajax() ){
?>
@section('jsbottom')
<?php
}
?>
	<script>
		modulo = {!! json_encode($_modulo) !!};
	</script>
	{!! Html::script('/backend/js/backend/'.$_modulo->sistema.'/'.$_modulo->nome.'.js') !!}
<?php
	if ( !Request::ajax() ){
?>
@stop
<?php
}
?>