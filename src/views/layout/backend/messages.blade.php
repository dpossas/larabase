@if ($errors->count() > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger fade in">
                	<button class="close" data-dismiss="alert">
                		×
                	</button>
                	<i class="fa-fw fa fa-check"></i>
                	<li>{{ $error }}</li>
                </div>
            @endforeach
        </ul>
    </div>
@endif
