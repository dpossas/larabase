<aside id="left-panel">
	<div class="login-info">
		<span>
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
				<img src="{!! URL::to('/backend/img/avatars/guest_sm.png') !!}" alt="me" class="online" /> 
				<span>
					{!! strlen(trim(Sentry::getUser()->nomeCompleto())) > 0 ? Sentry::getUser()->nomeCompleto() : strtolower(Sentry::getUser()->email) !!}
				</span>
				<i class="fa fa-angle-down"></i>
			</a> 
		</span>
	</div>
	
	<nav>
		<ul>
			<li class="">
				<a href="{!! URL::route('adm.home') !!}" title="{!! trans('menu.painel') !!}">
					<i class="fa fa-lg fa-fw fa-home"></i> 
					<span class="menu-item-parent">
						{!! trans('menu.painel') !!}
					</span>
				</a>
			</li>
			@if ( session('_s') )
			{!! Menu::renderizaMenu(session('_s')); !!}
			@else
			{!! Menu::renderizaMenu('basel5'); !!}
			@endif
		</ul>
	</nav>
	<span class="minifyme" data-action="minifyMenu"> 
	<i class="fa fa-arrow-circle-left hit"></i> 
	</span>
</aside>