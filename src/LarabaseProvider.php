<?php

namespace Dpossas\Larabase;

use Illuminate\Support\ServiceProvider;

class LarabaseProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //routes
        if (! $this->app->routesAreCached()) {
            require __DIR__.'/../../routes.php';
        }
    
        //loadviews
        $this->loadViewsFrom(__DIR__.'/views', 'larabase');
        
        $this->publishes([
            __DIR__.'/views' => resource_path('views/dpossas/larabase'),
        ]);
        
        //translations
        $this->loadTranslationsFrom(__DIR__.'/lang', 'larabase');
        
        $this->publishes([
            __DIR__.'/lang' => resource_path('lang/dpossas/larabase'),
        ]);
        
        //assets
        $this->publishes([
            __DIR__.'/assets' => public_path('dpossas/larabase'),
        ], 'public');
        
        //migrations
        $this->publishes([
            __DIR__.'/migrations' => database_path('migrations')
        ], 'migrations');
        
        //seeds
        $this->publishes([
            __DIR__.'/database/seeds' => database_path('seeds')
        ], 'seeds');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
