<?php

namespace App\Http\Controllers\Basel5;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use \Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Database\Eloquent\Collection;
use \App\Models\Basel5\User;

class ModuloController extends Controller
{
    use \ForbiddenTrait;

    private $modulo = null;
    private $tipo_opcoes = array();

    public function __construct(){
        //buscando o sistema basel5
        $base = \App\Models\Basel5\Sistema::where('nome','=','basel5')->first();

        $this->modulo = \App\Models\Basel5\Modulo::where('nome','=','modulo')
            ->where('sistema_id','=',$base->id)->firstOrFail();

        $this->tipo_opcoes = [
            'ME' => trans('modulotipomenu'),
            'CS' => trans('modulo.tipocadastrosimples'),
            'CC' => trans('modulo.tipocadastrocompleto'),
            'SE' => trans('modulo.tiposelecao'),
            'CI' => trans('modulo.tipocontroleinterno'),
            'CL' => trans('modulo.tipoclone')
        ];
    }
    
    public static function rotas()
    {
        return [
            ['method'=>'post','name'=>'data', 'alias'=>'dados'],
            ['method'=>'post','name'=>'modulos', 'alias'=>'modulos'],
            ['method'=>'post','name'=>'ordenarModulos', 'alias'=>'ordenarmodulos'],
        ];
    }
    
    public function ordenarModulos(Request $request)
    {
        if ( $request->get('dados') ){
            foreach( $request->get('dados') as $k => $d ){
                $item = \App\Models\Basel5\Modulo::find($d['id']);
                $item->ordem = $k;
                $item->save();
            }
        }
        
        return response('', 200);
    }

    public function modulos(Request $request = null, $sistema_id = null)
    {
        if ( $request && !$sistema_id )
            $sistema_id = $request->get('sistema_id');

        $listamodulos = \App\Models\Basel5\Modulo::select('basel5_modulo.titulo','basel5_modulo.id')
                ->where('basel5_sistema.id','=',$sistema_id)
                ->join('basel5_sistema','basel5_sistema.id','=','basel5_modulo.sistema_id')
                ->where('basel5_modulo.tipo','=','ME')->orderBy('basel5_modulo.titulo')
                ->lists('basel5_modulo.titulo','basel5_modulo.id')->all();
        $ret = [null=>trans('lbl.selecione')]+$listamodulos;

        return $ret;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_acesso') ) ){
            return view('backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index');
        } else {
            return $this->acessoNegado();
        }
    }
    
    public function data(Request $request)
    {
        if ( $request->get('t') === 'select2' ) {
            $result = Collection::make(DB::table('tabela')
                            ->select('basel5_modulo.id', 'basel5_modulo.descricao', 'basel5_modulo.titulo', 'basel5_modulo.nome', 'basel5_sistema.titulo as sistema')
                            ->join('basel5_sistema','basel5_sistema.id','=','basel5_modulo.sistema_id')
                            ->where('descricao','like','%'.$request->get('q').'%')
                            ->orderBy('descricao','asc')
                            ->get())->keyBy('id');

            echo json_encode($result);

            return;
        }

        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_acesso') ) ){
            $fields = array(
                'basel5_modulo.id',
                'basel5_modulo.descricao',
                'basel5_modulo.titulo',
                'basel5_modulo.nome',
                'basel5_sistema.titulo as sistema'
                );
            $all = \App\Models\Basel5\Modulo::select($fields)
                    ->join('basel5_sistema','basel5_sistema.id','=','basel5_modulo.sistema_id');
            if ( $request->get('descricao') )
                $all->where('descricao','like','%'.$request->get('descricao').'%');
            return \Datatables::of($all)
                ->make(true);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_criacao') ) ){
            $listasistemas = \App\Models\Basel5\Sistema::orderBy('nome')->orderBy('titulo')->lists('titulo','id');
            $listamodulos = array_merge([null=>trans('lbl.selecione')], \App\Models\Basel5\Modulo::where('sistema_id','=',$listasistemas->keys()[0])
                ->where('tipo','=','ME')->orderBy('titulo')->lists('titulo','id')->all());
            return view('backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')
                ->with([
                    'listasistemas'=>$listasistemas,
                    'listamodulos'=>$listamodulos,
                    'tipo_opcoes'=>$this->tipo_opcoes
                ]);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_criacao') ) ){
            $rules = array(
                'sistema_id'=>'required',
                'nome'=>'required',
                'titulo'=>'required',
                'tipo'=>'required|in:CC,CL,ME,CS,SE,CI',
                'ativo'=>'required|boolean',
                'menu'=>'required|boolean',
                'customizado'=>'required|boolean',
                'paginacao'=>'required|boolean',
                'icone'=>'required'
            );

            $validator = \Validator::make($request->all(), $rules);

            if ( $validator->fails() ){
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')),[],false))
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {
                    $item = new \App\Models\Basel5\Modulo;
                    $item->sistema_id = $request->get('sistema_id');
                    $item->modulo_id = $request->get('modulo_id') ?: null;
                    $item->nome = $request->get('nome');
                    $item->titulo = $request->get('titulo');
                    $item->descricao = $request->get('descricao');
                    $item->tabela = $request->get('tabela');
                    $item->tipo = $request->get('tipo');
                    $item->ativo = $request->get('ativo');
                    $item->menu = $request->get('menu');
                    $item->customizado = $request->get('customizado');
                    $item->destino = $request->get('destino');
                    $item->janela_largura = $request->get('janela_largura') ?: null;
                    $item->janela_altura = $request->get('janela_altura') ?: null;
                    $item->paginacao = $request->get('paginacao');
                    $item->paginacao_registros = $request->get('paginacao_registros');
                    $item->modulo_clonagem_id = $request->get('modulo_clonagem_id');
                    $item->modulo_substituido_id = $request->get('modulo_substituido_id');
                    $item->nova_janela = $request->get('nova_janela') ?: false;
                    $item->icone = $request->get('icone');
                    $item->user_create_id = Sentry::getUser() && Sentry::getUser()->id ? Sentry::getUser()->id : User::firstOrFail()->id;
                    $item->user_update_id = Sentry::getUser() && Sentry::getUser()->id ? Sentry::getUser()->id : User::firstOrFail()->id;
                    $item->save();

                    return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false))
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } catch ( Exception $e ){
                    return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.create')),[],false))
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //acao não permitida
        return $this->acessoNegado();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $item = \App\Models\Basel5\Modulo::find($id);
            $listasistemas = \App\Models\Basel5\Sistema::orderBy('nome')->orderBy('titulo')->lists('titulo','id');
            $listamodulos = \App\Models\Basel5\Modulo::where('sistema_id','=',$item->sistema_id)
                ->where('tipo','=','ME')->orderBy('titulo')->lists('titulo','id')->all();
            array_unshift($listamodulos,[null=>trans('lbl.selecione')]);

            return view( 'backend.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit')
                ->with([
                    'item'=>$item,
                    'listasistemas'=>$listasistemas,
                    'listamodulos'=>$listamodulos,
                    'tipo_opcoes'=>$this->tipo_opcoes
                ]);
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_edicao') ) ){
            $rules = array(
                'sistema_id'=>'required',
                'nome'=>'required',
                'titulo'=>'required',
                'tipo'=>'required|in:CC,CL,ME,CS,SE,CI',
                'ativo'=>'required|boolean',
                'menu'=>'required|boolean',
                'customizado'=>'required|boolean',
                'paginacao'=>'required|boolean',
                'icone'=>'required'
            );

            $validator = \Validator::make($request->all(), $rules);
            
            if ( $validator->fails() ){
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit',$id)),[],false))
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try {
                    $item = \App\Models\Basel5\Modulo::find($id);
                    $item->sistema_id = $request->get('sistema_id');
                    $item->modulo_id = $request->get('modulo_id') ?: null;
                    $item->nome = $request->get('nome');
                    $item->titulo = $request->get('titulo');
                    $item->descricao = $request->get('descricao');
                    $item->tabela = $request->get('tabela');
                    $item->tipo = $request->get('tipo');
                    $item->ativo = $request->get('ativo');
                    $item->menu = $request->get('menu');
                    $item->customizado = $request->get('customizado');
                    $item->destino = $request->get('destino');
                    $item->janela_largura = $request->get('janela_largura') ?: null;
                    $item->janela_altura = $request->get('janela_altura') ?: null;
                    $item->paginacao = $request->get('paginacao');
                    $item->paginacao_registros = $request->get('paginacao_registros');
                    $item->modulo_clonagem_id = $request->get('modulo_clonagem_id');
                    $item->modulo_substituido_id = $request->get('modulo_substituido_id');
                    $item->nova_janela = $request->get('nova_janela') ?: false;
                    $item->icone = $request->get('icone');
                    $item->user_update_id = Sentry::getUser() && Sentry::getUser()->id ? Sentry::getUser()->id : User::firstOrFail()->id;
                    $item->save();

                    $request->session()->flash('success', trans('messages.savesuccess'));

                    return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false));
                } catch ( Exception $e ){
                    return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.edit',$id)),[],false))
                        ->withErrors([trans('messages.saveerror')])
                        ->withInput() ;
                } 
            }
        } else {
            return $this->acessoNegado();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Sentry::getUser() && ( Sentry::getUser()->isSuperUser() || Sentry::getUser()->hasPermission($this->modulo->nome.'_exclusao') ) ){
            $item = \App\Models\Basel5\Modulo::find($id);
            
            if ( !$item ){
                $request->session()->flash('error', trans('messages.notfound'));
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false))
                        ->withInput($request->except('password'));
            } else {
                if ( !$item->delete() ){
                    $request->session()->flash('error', trans('messages.deleteerror'));
                } else {
                    $request->session()->flash('success', trans('messages.deletesuccess'));
                }
                return redirect(route('adm.home',[],false).'/#'.route(strtolower(camel_case('adm.'.$this->modulo->sistema()->nome.'.'.$this->modulo->nome.'.index')),[],false));
            }
        } else {
            return $this->acessoNegado();
        }
    }
}
